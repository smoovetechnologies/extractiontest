import models
import typing

from rich.table import Table
from companyloggers.richie import create_table

from langchain_openai.llms.base import BaseLLM
from langchain_openai import AzureOpenAI, AzureChatOpenAI


class AzureLangchainGPTClient:
    def __init__(
        self,
        credential_config: models.AzureGPTCredentialModel,
        model_configs: typing.List[models.AzureGPTInstanceModel],
    ):
        self.credential_config: models.AzureGPTCredentialModel = credential_config
        self.models_config: typing.Dict[str, models.AzureGPTInstanceModel] = {
            model.name: model for model in model_configs
        }

        self.models = {}

    def get_model(self, model_name: str) -> BaseLLM:
        """
        Lazy initialization and retrieval of model instances.
        """
        if model_name not in self.models:
            if model_name in self.models_config:
                model_config: models.AzureGPTInstanceModel = self.models_config[
                    model_name
                ]
                self.models[model_name]: BaseLLM = self._create_instance(
                    model_config=model_config
                )
            else:
                raise ValueError("Model not configured.")
        return self.models[model_name]

    def list_models(self) -> Table:
        """
        Returns a table of available models.
        """
        return list(self.models_config.keys())

    def get_model_table(self) -> Table:
        """
        Returns a table of available models.
        """

        columns: typing.List[typing.List[str]] = [
            [
                value.name,
                value.deployment_name,
                str(value.max_tokens),
                str(value.streaming),
            ]
            for _, value in self.models_config.items()
        ]

        table: Table = create_table(
            title="Available Models",
            columns=["Model Name", "Deployment Name", "Max Tokens", "Streaming"],
            rows=columns,
        )

        return table

    def _create_instance(self, model_config: models.AzureGPTInstanceModel) -> BaseLLM:
        """
        Decides the type of model instance to create based on the model configuration.
        """
        if (
            "4" in model_config.deployment_name
        ):  # Assuming model version can be inferred from deployment_name
            return self._create_chat_instance(model_config=model_config)
        else:
            return self._create_model_instance(model_config=model_config)

    def _create_model_instance(
        self, model_config: models.AzureGPTInstanceModel
    ) -> BaseLLM:
        """
        Factory method to create general Azure AI model instances.
        """
        return AzureOpenAI(
            api_key=self.credential_config.API_KEY,
            azure_endpoint=self.credential_config.API_URL,
            api_version=self.credential_config.API_VERSION,
            deployment_name=model_config.deployment_name,
            streaming=model_config.streaming,
            max_tokens=model_config.max_tokens,
        )

    def _create_chat_instance(
        self, model_config: models.AzureGPTInstanceModel
    ) -> BaseLLM:
        """
        Factory method to create chat-specific Azure AI model instances.
        """
        return AzureChatOpenAI(
            api_key=self.credential_config.API_KEY,
            azure_endpoint=self.credential_config.API_URL,
            api_version=self.credential_config.API_VERSION,
            deployment_name=model_config.deployment_name,
            streaming=model_config.streaming,
            max_tokens=model_config.max_tokens,
        )
