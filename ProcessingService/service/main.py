from configs import env_config
import configs.env_config  # Has to be imported first to setup the environment variables

import logging

from fastapi import FastAPI

from companywrappers import abstract
from companyloggers import LoguruLogger
from companyconfigs import Operation, Environment

import configs
import service

# Define the logger for this service
# The options are rich and loguru or both combined
loggie: LoguruLogger = LoguruLogger(
    level=logging.DEBUG,
)
loggie.add_logger()

# Define how the service will operation
operation: Operation = Operation.API

# Create the app
app: FastAPI = FastAPI(
    title=configs.service_config.service_name.replace("_", " ").title()
)  # Needs to be in the global scope for FastAPI to work

# Setup the service
if operation == Operation.API:
    service_setup: abstract.AbstractBaseService = service.setup_api(app=app)
elif operation == Operation.BROKER:
    service_setup: abstract.AbstractBaseService = service.setup_broker()


# Start the service
if __name__ == "__main__":
    # Log the service configuration
    configs.config.log_service_configuration(
        base_url=configs.base_url,
        environment=str(configs.system.environment),
        version=configs.system.version,
        life_cycle=configs.system.life_cycle,
    )

    # Start the service
    service_setup.start()

# TODO: understand how this would work in the cloud
