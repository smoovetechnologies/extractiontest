from companypackout import (
    DocumentIntelligenceEngine,
    FromPathDocumentIntelligenceOCREngine,
    DocumentIntelligenceLoadingEngine,
    DocumentIntelligencePrebuiltModelsConfig,
)

import engines
import configs
import clients
import abstract

from .initialised_clients import embedding_client, chat_client, gpt_35_turbo, gpt_4

# Input validation engine
input_validation_engine: engines.InputValidationEngine = engines.InputValidationEngine()

# This provides the document type identification capabilities
document_identification_engine: engines.DocumentIdentificationEngine = (
    engines.DocumentIdentificationEngine()
)

# OCR
document_intelligence_engine: DocumentIntelligenceEngine = DocumentIntelligenceEngine(
    endpoint=configs.embedding_creation_pipeline_config.document_intelligence_endpoint,
    key=configs.embedding_creation_pipeline_config.document_intelligence_key,
)
ocr_from_path_engine: FromPathDocumentIntelligenceOCREngine = (
    FromPathDocumentIntelligenceOCREngine(
        engine=document_intelligence_engine,
        supported_file_types=[
            value.value
            for _, value in configs.SupportedOCRTypeConig.__members__.items()
        ],
    )
)


document_intelligence_loading_engine: DocumentIntelligenceLoadingEngine = (
    DocumentIntelligenceLoadingEngine()
)

# Embedding engine provides the embedding capabilities
embedding_engine: engines.EmbeddingEngine = engines.EmbeddingEngine(
    embedding_client=embedding_client,
    chat_client=chat_client,
    embedding_model=configs.embedding_creation_pipeline_config.embedding_model,
    chat_model=configs.embedding_creation_pipeline_config.chat_model,
)


# Agents
gpt_4_agent: abstract.AbstractAgent = engines.AgentEngine(llm=gpt_4)
gpt_35_turbo_agent: abstract.AbstractAgent = engines.AgentEngine(llm=gpt_35_turbo)

# Parsing
