import configs
import pipelines

from .initialised_engines import (
    input_validation_engine,
    document_identification_engine,
    document_intelligence_engine,
    embedding_engine,
)

from .initialised_clients import (
    chat_client,
    embedding_client,
    azure_blob_client,
    azure_cosmonru_client,
)

# Pipelines
# TODO: think of a better way to inject the configs into the pipeline

embedding_creation_pipeline: pipelines.EmbeddingCreationPipeline = (
    pipelines.EmbeddingCreationPipeline(
        embedding_model=configs.embedding_creation_pipeline_config.embedding_model,
        chat_model=configs.embedding_creation_pipeline_config.chat_model,
        input_validation_engine=input_validation_engine,
        document_identification_engine=document_identification_engine,
        document_analyser_engine=document_intelligence_engine,
        embedding_engine=embedding_engine,
        chat_client=chat_client,
        embedding_client=embedding_client,
        azure_cosmonru_client=azure_cosmonru_client,
        azure_blob_client=azure_blob_client,
    )
)
