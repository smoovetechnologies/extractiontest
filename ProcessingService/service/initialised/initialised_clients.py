from langchain_openai.llms.base import BaseLLM

from companyclients import azure_clients, db_clients

import configs
import clients
import models

# Azure
# Blob client
azure_blob_connection_client: azure_clients.AzureBlobConnectionClient = (
    azure_clients.AzureBlobConnectionClient(
        connection_string=configs.azure_blob_config["CONNECTION_STRING"]
    )
)
azure_blob_client: azure_clients.AzureBlobClient = azure_clients.AzureBlobClient(
    client=azure_blob_connection_client.client
)

# Cosmon client
azure_cosmonru_connection_client: azure_clients.AzureCosmonConnectionClient = (
    azure_clients.AzureCosmonConnectionClient(
        connection_string=configs.azure_cosmon_config["CONNECTION_STRING"]
    )
)
azure_cosmonru_client: azure_clients.AzureCosmonClient = (
    azure_clients.AzureCosmonClient(
        client=azure_cosmonru_connection_client.client,
        database=configs.azure_cosmon_config["DATA"],
    )
)

# GPT Clients
# Chat client
chat_client: azure_clients.AzureGPTClient = azure_clients.AzureGPTClient(
    api_key=configs.embedding_creation_pipeline_config.gpt_api_key,
    api_url=configs.embedding_creation_pipeline_config.gpt_api_url,
    api_type=configs.embedding_creation_pipeline_config.gpt_api_type,
    api_version=configs.embedding_creation_pipeline_config.gpt_api_version,
    deployment_name=configs.embedding_creation_pipeline_config.chat_model,
)
# Embedding client
embedding_client: azure_clients.AzureGPTClient = azure_clients.AzureGPTClient(
    api_key=configs.embedding_creation_pipeline_config.gpt_api_key,
    api_url=configs.embedding_creation_pipeline_config.gpt_api_url,
    api_type=configs.embedding_creation_pipeline_config.gpt_api_type,
    api_version=configs.embedding_creation_pipeline_config.gpt_api_version,
    deployment_name=configs.embedding_creation_pipeline_config.embedding_model,
)

# Redis Client
redis_client: db_clients.RedisClient = db_clients.RedisClient(
    host=configs.redis_cache_connection_config["HOST"],
    port=configs.redis_cache_connection_config["PORT"],
)

# SFTP Client
# sftp_client: db_clients.SFTPClient = db_clients.SFTPClient(
#     host=configs.sftp_config["HOST"],
#     port=configs.sftp_config["PORT"],
#     username=configs.sftp_config["USERNAME"],
#     password=configs.sftp_config["PASSWORD"],
# )


# GPT clients via Azure Langchain
azure_langchain_gpt_client: clients.AzureLangchainGPTClient = (
    clients.AzureLangchainGPTClient(
        credential_config=models.AzureGPTCredentialModel(
            **configs.azure_gpt_api_credentials_config
        ),
        model_configs=[
            configs.AzureGPTDeploymentConfig.GPT35Turbo,
            configs.AzureGPTDeploymentConfig.GPT4,
        ],
    )
)

gpt_35_turbo: BaseLLM = azure_langchain_gpt_client.get_model(
    model_name=configs.AzureGPTDeploymentConfig.GPT35Turbo.name
)
gpt_4: BaseLLM = azure_langchain_gpt_client.get_model(
    model_name=configs.AzureGPTDeploymentConfig.GPT4.name
)
