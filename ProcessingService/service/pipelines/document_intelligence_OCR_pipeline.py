import pathlib


class DocumentIntelligenceOCRPipeline:

    def __init__(
        self,
        model_id: str,
        file_path: pathlib.Path = None,
        file_url: str = None,
    ):
        """OCR via Document Intelligence constructor."""

        self.model_id: str = model_id
        self.file_path: pathlib.Path = file_path
        self.file_url: str = file_url
