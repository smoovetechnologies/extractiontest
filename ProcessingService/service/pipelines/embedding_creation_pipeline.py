import json
import shutil
import typing
import pathlib
import datetime

from loguru import logger as log

from companyconfigs import Environment
from companyclients import azure_clients, db_clients
from companypackout import DocumentIntelligenceEngine
from companymodels import processing_models
from companyutilities import io_utilities

import engines
import configs


class EmbeddingCreationPipeline:
    def __init__(
        self,
        embedding_model: str,
        chat_model: str,
        input_validation_engine: engines.InputValidationEngine,
        document_identification_engine: engines.DocumentIdentificationEngine,
        document_analyser_engine: DocumentIntelligenceEngine,
        embedding_engine: engines.EmbeddingEngine,
        chat_client: azure_clients.AzureGPTClient,
        embedding_client: azure_clients.AzureGPTClient,
        azure_cosmonru_client: azure_clients.AzureCosmonClient,
        azure_blob_client: azure_clients.AzureBlobClient,
        sftp_client: db_clients.SFTPClient = None,
        testing: bool = False,
    ):
        # Instance variables
        self.testing: bool = testing  # Testing flag for testing during development

        # Define the models to be used in the embedding creation pipeline
        self.embedding_model: str = embedding_model
        self.chat_model: str = chat_model

        # Engines
        self.input_validation_engine: engines.InputValidationEngine = (
            input_validation_engine
        )
        self.document_identification_engine: engines.DocumentIdentificationEngine = (
            document_identification_engine
        )
        self.document_analyser_engine: DocumentIntelligenceEngine = (
            document_analyser_engine
        )
        self.embedding_engine: engines.EmbeddingEngine = embedding_engine

        # Clients
        self.chat_client: azure_clients.AzureGPTClient = chat_client
        self.embedding_client: azure_clients.AzureGPTClient = embedding_client
        self.azure_cosmonru_client: azure_clients.AzureCosmonClient = (
            azure_cosmonru_client
        )
        self.azure_blob_client: azure_clients.AzureBlobClient = azure_blob_client
        self.sftp_client: db_clients.SFTPClient = sftp_client

    def read_from_sftp(
        self,
        filename: str,
        remote_path: pathlib.Path,
    ):
        """
        Read a file from the SFTP server and save it to the local storage.

        Args:
            filename (str): Name of the file.
            remote_path (pathlib.Path): Path to the file on the SFTP server.

        """

        local_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / filename
        log.info(f"Reading file from SFTP at {remote_path} and saving to {local_path}.")

        try:
            self.sftp_client.read_from_server(
                remote_path=pathlib.Path(remote_path),
                local_path=local_path,
            )

            return local_path

        except Exception as e:
            log.error(f"Failed to read from SFTP server: {e}")
            raise e

    def identify_ocr_model(
        self,
        filename: str,
    ) -> str:
        """
        Identify the model to use in the OCR-FR.

        Args:
            filename (str): Name of the file.

        Returns:
            str: The model ID to use in the document analyser.

        """

        log.info(f"Beginning document type identification for {filename}.")

        model_id: str = self.document_identification_engine.identify()

        model_id: str = (
            configs.embedding_creation_pipeline_config.document_intelligence_model_id
        )
        log.success(f"Model ID {model_id} will be used for {filename}.")

        return model_id

    def perform_ocr(
        self,
        filename: str,
        model_id: str,
        file_path: pathlib.Path = None,
        file_url: str = None,
    ) -> processing_models.DocumentAnalysisModel:
        """
        Perform OCR on the document.
        This uses the document analyser engine to perform OCR on the document.
        The document analyser is not initialised in the method because some class attributes must be set first.

        Args:
            filename (str): Name of the document.
            model_id (str): Model ID to use in the document analyser.
            file_path (pathlib.Path, optional): Path to the document. Defaults to None.
            file_url (str, optional): URL to the document. Defaults to None.

        Returns:
            processing_models.DocumentAnalysisModel: OCR result.

        """

        log.info(f"Beginning OCR for the document {filename}.")

        filename_json: str = filename.split(".")[0] + ".json"
        container_name: str = configs.azure_blob_config["OCR_RESULTS_CONTAINER_NAME"]
        # Check the OCR result doesnt exist in the Cosmos database
        # If it does, return the result from there
        analyse_result: bool = self.azure_blob_client.check_blob_exists(
            container_name=container_name,
            blob_name=filename_json,
        )

        if analyse_result:
            log.info(
                f"OCR result for {filename} already exists in the blob storage. Returning the result."
            )
            self.azure_blob_client.download_blob(
                container_name=container_name,
                blob_name=filename_json,
                destination_path=configs.PathConfig.TEMPORARY_STORAGE / filename,
            )

            loader: engines.DocumentIntelligenceModelLoadingEngine = (
                engines.DocumentIntelligenceModelLoadingEngine()
            )
            analyse_result: processing_models.DocumentAnalysisModel = (
                loader.load_from_path(
                    path=configs.PathConfig.TEMPORARY_STORAGE / filename
                )
            )

            io_utilities.create_directory(path=configs.PathConfig.TEMPORARY_STORAGE)

            # Remove the local JSON file
            # (configs.PathConfig.TEMPORARY_STORAGE / filename).unlink()

            return analyse_result

        if file_path is not None:
            analyse_result: processing_models.DocumentAnalysisModel = (
                self.document_analyser_engine.analyse_from_path(
                    path=file_path,
                    model_id=model_id,
                )
            )
        elif file_url is not None:
            analyse_result: processing_models.DocumentAnalysisModel = (
                self.document_analyser_engine.analyse_from_url(
                    url=file_url,
                    model_id=model_id,
                )
            )

        # Create a JSON file from the OCR result
        local_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / filename_json
        io_utilities.save_json(data=analyse_result.to_dict(), output_path=local_path)

        # Upload the JSON file to the blob storage
        self.azure_blob_client.upload_blob(
            container_name=container_name,
            blob_name=filename_json,
            file_path=local_path,
        )

        # Remove the local JSON file
        local_path.unlink()

        log.success(f"OCR complete for document {filename}.")
        return analyse_result

    def preprocess_to_string(
        self,
        filename: str,
        analyse_result: processing_models.DocumentAnalysisModel,
        prefix: bool = False,
        data_subset: int = -1,
    ) -> str:
        """
        Preprocess the OCR-FR result and generate a structured string.

        Args:
            filename (str): Name of the document.
            analyse_result (processing_models.DocumentAnalysisModel): OCR result.
            prefix (bool, optional): Whether to add a prefix to the text. Defaults to False.
            data_subset (int, optional): The number of paragraphs to process. Defaults to -1.

        Returns:
            str: Preprocessed data.

        """

        log.info(f"Preprocessing text for {filename}.")

        # Define the segmenting preprocessing engine
        preprocessing_engine: engines.SegmentingPreprocessingToStringEngine = (
            engines.SegmentingPreprocessingToStringEngine(
                filename=filename,
                analyse_result=analyse_result,
                prefix=prefix,
                data_subset=data_subset,
            )
        )

        preprocessed_data: str = preprocessing_engine.preprocess_data()

        preprocessing_engine.log_data_metrics()
        preprocessing_engine.log_sections_and_content()

        # Create a langchain segmenting engine here

        return preprocessed_data

    def generate_segments_from_result(
        self,
        filename: str,
        analyse_result: processing_models.DocumentAnalysisModel,
        data_subset: int = -1,
    ) -> typing.List[processing_models.SegmentModel]:
        """
        Generate segments from the OCR-FR result.
        No preprocessing is required for this method.

        Args:
            filename (str): Name of the document.
            analyse_result (processing_model.DocumentAnalysisModel): OCR result.
            data_subset (int, optional): The number of paragraphs to process. Defaults to -1.

        Returns:
            typing.List[processing_models.SectionModel]: Preprocessed data.

        """

        log.info(f"Preprocessing text for {filename}.")

        segmenting_engine: engines.SegmentingEngine = engines.SegmentingEngine(
            filename=filename,
            analyse_result=analyse_result,
            chat_client=self.chat_client,
            chat_model=self.chat_model,
            data_subset=data_subset,
        )

        preprocessed_data: typing.List[processing_models.SegmentModel] = (
            segmenting_engine.segment_data()
        )

        segmenting_engine.log_data()

        return preprocessed_data

    def generate_segments_from_string(
        self,
        filename: str,
        preprocessed_data: str,
        segment_size: typing.Optional[int] = None,
        segment_overlap: typing.Optional[int] = None,
    ) -> typing.List[processing_models.SegmentModel]:
        """
        Generate segments from a string.
        This requires a preprocessed step to convert the OCR-FR result to a string.

        Args:
            filename (str): Name of the document.
            preprocess_data (typing.Union[str, typing.List[typing.Dict[str, str]]]): Preprocessed data.
            segment_size (typing.Optional[int], optional): Size of each segment. Defaults to None.
            segment_overlap (typing.Optional[int], optional): Overlap between each segment. Defaults to None.

        Returns:
            typing.List[processing_models.SegmentModel]: List of segments.

        """

        log.info(f"Beginning segment generation for {filename}.")

        segmenting_engine: engines.LangchainSegmentingEngine = (
            engines.LangchainSegmentingEngine(
                segment_size=segment_size,
                segment_overlap=segment_overlap,
            )
        )

        segments: typing.List[processing_models.SegmentModel] = (
            segmenting_engine.segment_data(
                filename=filename,
                text_string=preprocessed_data,
            )
        )

        return segments

    def generate_embeddings(
        self,
        file_data: processing_models.FileModel,
        segments: typing.List[processing_models.SegmentModel],
    ):
        """
        Generate the embeddings for the segments.
        The method handles the process of generating the embeddings for each segment.
        The method will iterate over the segments, create a defined text string for each content in a segment and generate the embeddings.
        The embeddings are then stored as a list in a defined data model.

        Args:
            file_data (processing_models.FileModel): The file data.
            segments (typing.List[processing_models.SegmentModel]): The segments of the text.

        Returns:
            typing.List[processing_models.EmbeddingVectorModel]: List of embedding vectors.

        """
        log.info(f"Beginning embedding generation for {file_data.filename}.")

        # Input validation for the segments
        self.input_validation_engine.validate_segments(segments=segments)

        embedding_vectors: typing.List[processing_models.EmbeddingVectorModel] = []

        counter: int = 0

        for segment in segments:
            for content in segment.content:
                text: str = (
                    configs.EmbeddingConfig.EMBEDDING_GENERATION_TEXT_FORMAT.format(
                        title=segment.title,
                        section_heading=content.section_heading,
                        text=content.text,
                    )
                )

                embedding: typing.List[float] = (
                    self.embedding_engine.generate_embeddings(text=text)
                )

                # Unpack the data from the file data
                embedding_vector: processing_models.EmbeddingVectorModel = (
                    processing_models.EmbeddingVectorModel(
                        **file_data.dict(),
                        _id=counter,
                        id=counter,
                        title=segment.title,
                        section_heading=content.section_heading,
                        text=content.text,
                        embedding=embedding,
                        embedding_model=self.embedding_model,
                        word_count=content.word_count,
                        token_count=content.token_count,
                        bounding_regions=content.bounding_regions,
                        creation_date=datetime.datetime.now().isoformat(),
                        last_modified_date=datetime.datetime.now().isoformat(),
                    )
                )

                embedding_vectors.append(embedding_vector)

                counter += 1

        log.success(f"Embedding generation complete for {file_data.filename}.")

        return embedding_vectors

    def postprocess_embeddings(
        self,
        filename: str,
        analyse_result: processing_models.DocumentAnalysisModel,
        text_segments: typing.List[processing_models.SegmentModel],
        embedding_vectors: typing.List[processing_models.EmbeddingVectorModel],
    ):
        """
        Post process the embeddings. Take the OCR-FR result, the text segments and the embedding vectors and map the sentences.
        This is only required if the segments were generated from a string.

        Args:
            filename (str): The name of the file.
            analyse_result (processing_models.DocumentAnalysisModel): The result of the OCR-FR analysis.
            text_segments (typing.List[processing_models.SegmentModel]): The segments of the text.
            embedding_vectors (typing.List[processing_models.EmbeddingVectorModel]): The embeddings of the segments.

        """

        log.info(f"Beginning post processing for {filename} data.")

        # Input validation for the segments and embedding vectors
        self.input_validation_engine.validate_segments(segments=text_segments)
        self.input_validation_engine.validate_embedding_vectors(
            embedding_vectors=embedding_vectors
        )

        segmenting_postprocessing_engine: engines.SegmentingPostprocessingEngine = (
            engines.SegmentingPostprocessingEngine(
                analyse_result=analyse_result,
                text_segments=text_segments,
                embedding_vectors=embedding_vectors,
            )
        )

        segmenting_postprocessing_engine.postprocess()
        log.success(f"Post processing complete for {filename} data.")

    def remove_current_embeddings(
        self,
        filename: str,
    ):
        """
        Remove the current embeddings for a file from Cosmos.

        Args:
            filename (str): The name of the file.

        """

        log.info(f"Removing the current embeddings for {filename} from Cosmos.")

        # Remove the current embeddings from Cosmos
        self.azure_cosmonru_client.delete_documents(
            collection_name=configs.azure_cosmon_config["EMBEDDINGS"],
            query={"filename": filename},
        )

    def push_embeddings(
        self,
        filename: str,
        embedding_vectors: typing.List[processing_models.EmbeddingVectorModel],
    ):
        """
        Push the embeddings to Cosmos.

        Args:
            filename (str): The name of the file.
            embedding_vectors (typing.List[processing_models.EmbeddingVectorModel]): List of embedding vectors.

        """
        log.info(f"Pushing the embeddings for {filename} to Cosmos.")

        # Input validation for the embedding vectors
        self.input_validation_engine.validate_embedding_vectors(
            embedding_vectors=embedding_vectors
        )

        # Convert the embedding vectors to a list of dictionaries
        embedding_vectors: typing.List[typing.Dict] = [
            embedding_vector.dict() for embedding_vector in embedding_vectors
        ]
        log.info(f"Found {len(embedding_vectors)} embeddings for {filename}.")

        # Create a list of a lists of dictionaries with 20 items per list to push to Cosmos rather than pushing all of them at once
        # This is to avoid the limit of the Cosmos API
        entries_per_list: int = 30
        embedding_vectors_list_of_lists: typing.List[typing.List[typing.Dict]] = [
            embedding_vectors[i : i + entries_per_list]
            for i in range(0, len(embedding_vectors), entries_per_list)
        ]

        log.info(
            f"Breaking the embedding list into {len(embedding_vectors_list_of_lists)} lists of {entries_per_list} items."
        )
        log.info("This is to avoid the limit of the Cosmos API.")

        # Push the list of lists to Cosmos
        pushed_count: int = 0
        for index, embedding_vectors_list in enumerate(embedding_vectors_list_of_lists):
            log.info(
                f"Pushing list {index + 1} of {len(embedding_vectors_list_of_lists)}, list length: {len(embedding_vectors_list)}"
            )
            # Push the embeddings to Cosmos
            self.azure_cosmonru_client.create_documents(
                collection_name=configs.azure_cosmon_config["EMBEDDINGS"],
                documents=embedding_vectors_list,
            )
            pushed_count += len(embedding_vectors_list)

        log.success(f"Pushed {pushed_count} embeddings for {filename} to Cosmos.")
