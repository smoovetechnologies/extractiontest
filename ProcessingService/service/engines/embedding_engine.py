import typing

from loguru import logger as log

from openai.types.chat import ChatCompletion

from companyclients import azure_clients


class EmbeddingEngine:
    """
    Class which converts segments to vector embeddings.

    The embeddings are generated using the OpenAI Embedding Model.

    OpenAI Embedding Model Documentation:
    https://platform.openai.com/docs/guides/embeddings

    old model: text-embedding-ada-002
    new small model: text-embedding-3-small
    new large model: text-embedding-3-large
    """

    def __init__(
        self,
        embedding_client: azure_clients.AzureGPTClient,
        chat_client: azure_clients.AzureGPTClient,
        embedding_model: str,
        chat_model: str,
    ) -> None:
        """Embedding engine constructor.

        Args:
            embedding_client (clients.AzureGPTClient): Embedding client is used to generate embeddings.
            chat_client (clients.AzureGPTClient): Chat client is used to summarise text if needed.
            embedding_model (str): The GPT model used to generate embeddings must be specified.
            chat_model (str): The GPT model used to summarise text must be specified.

        """

        self.embedding_client: azure_clients.AzureGPTClient = (
            embedding_client  # Embedding client is used to generate embeddings
        )
        self.chat_client: azure_clients.AzureGPTClient = (
            chat_client  # Chat client is used to summarise text if needed
        )
        self.chat_model: str = (
            chat_model  # The GPT model used to summarise text must be specified
        )
        self.embedding_model: str = (
            embedding_model  # The embedding model must be specified
        )

    def generate_embeddings(
        self,
        text: str,
    ) -> typing.List[float]:
        """
        Get the embeddings for the given text.
        Using the embedding client and the embedding model, generate the embeddings for the given text.

        Args:
            text (str): Text to generate embeddings for.

        Returns:
            typing.List[float]: List of embeddings.

        """

        embedding: typing.List[float] = (
            self.embedding_client.client.embeddings.create(
                input=text,
                model=self.embedding_model,
            )
            .data[0]
            .embedding
        )

        return embedding
