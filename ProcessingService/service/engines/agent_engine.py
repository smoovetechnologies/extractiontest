import abc
import typing

from loguru import logger as log

from langchain_openai.llms.base import BaseLLM
from langchain_core.prompts import PromptTemplate
from langchain_core.messages import AIMessage, BaseMessage
from langchain_core.language_models.base import BaseLanguageModel

from langchain.chains.llm import LLMChain

import abstract


class AgentEngine(abstract.AbstractAgent):
    def __init__(
        self,
        llm: BaseLanguageModel | BaseLLM,
        system_prompt: typing.Optional[PromptTemplate] = None,
    ):

        self.llm: BaseLanguageModel | BaseLLM = llm
        self.system_prompt: PromptTemplate = system_prompt

        if system_prompt is not None:
            self.chain = llm | system_prompt

    def stream_chain(
        self,
        chain: LLMChain,
        message_input: typing.Dict[str, typing.List[BaseMessage]],
    ) -> str:
        """Processes text using the specified chain and message input."""

        text: str = ""
        try:
            for chunk in chain.stream(message_input):
                text += chunk.content if isinstance(chunk, AIMessage) else chunk
        except Exception as e:
            log.error(f"Failed during processing with {chain}: {e}")
            raise e
        return text

    def invoke_chain(
        self,
        chain: LLMChain,
        message_input: typing.Dict[str, typing.List[BaseMessage]],
    ) -> str:
        """Processes text using the specified chain and message input."""

        text: str = ""
        try:
            text = chain.invoke(message_input)
        except Exception as e:
            log.error(f"Failed during processing with {chain}: {e}")
            raise e
        return text

    def stream_model(self, text: str) -> str:
        """Streams the model based on the provided state messages."""
        log.info("Streaming model")

        text = self.llm.stream(text)
        return text.content

    def invoke_model(self, text: str) -> str:
        """Generates a response based on the provided state messages."""
        log.info("Invoking model")

        text = self.llm.invoke(text)
        return text.content
