import typing

from rich.table import Table
from rich.panel import Panel
from rich.columns import Columns
from loguru import logger as log
from companyloggers.richie import create_table
from companyloggers.richie import log as richie

from openai.types.chat import ChatCompletion

from companyclients import azure_clients
from companymodels import processing_models

import configs
import abstract

from .utilities import SegmentingUtilities


class SegmentingEngine(
    abstract.AbstractSegmenter,
    SegmentingUtilities,
):
    """
    SegmentingEngine converts the raw OCR-FR JSON data into a data model format with the following structure:
        - Title: padded by triple newlines
        - Section heading: padded by double newlines
        - Text: padded by single newline

    """

    def __init__(
        self,
        filename: str,
        analyse_result: processing_models.DocumentAnalysisModel,
        chat_client: azure_clients.AzureGPTClient,
        chat_model: str,
        data_subset: int = -1,
    ):
        """
        SegmentingEngine constructor.

        Args:
            filename (str): The filename of the document.
            analyse_result (processing_models.DocumentAnalyserEngine): The result of the OCR-FR analysis.
            data_subset (int, optional): The number of paragraphs to process. Defaults to -1.

        """
        # Initialise the parent class
        super().__init__(
            filename=filename,
            analyse_result=analyse_result,
        )

        self.filename: str = filename
        self.analyse_result: processing_models.DocumentAnalysisModel = analyse_result

        # Chat client is used to summarise and clean the text if needed
        self.chat_client: azure_clients.AzureGPTClient = (
            chat_client  # Chat client is used to summarise text if needed
        )
        self.chat_model: str = (
            chat_model  # The GPT model used to summarise text must be specified
        )

        # Extract relevant data and generate formatted text or structured JSON
        self.data_subset: int = data_subset

        self.segments: typing.List[processing_models.SegmentModel] = []
        self.segment: processing_models.SegmentModel = None
        self.content: processing_models.ContentModel = None

        self.current_title: str = None
        self.current_subheading: str = None

        self.default_section_heading: str = "N/A"  # Default section heading
        self.minimum_sentence_length: int = 4  # Minimum sentence length

        # Counters
        self.section_counter: int = 0
        self.content_counter: int = 0

    def segment_data(
        self,
    ) -> typing.List[processing_models.SegmentModel]:
        """
        Segment the document into a structured data model.

        Returns:
            typing.List[processing_models.SegmentModel]: The structured data model.

        """

        # Preprocess the paragraphs and generate the data model
        # self._segment_paragraphs()
        self._segment_pages()

        # Clean the segments
        # self._postprocess_segments()

        return self.segments

    def log_data(self):
        """Log the data for the document."""

        log.info(f"Logging the data for the document: {self.filename}")

        # Log the data metrics
        data_metrics_table: Table = self.log_data_metrics()

        # Log the sections and content
        sections_and_content_table: Table = self._log_sections_and_content()
        # Log the token counts
        token_counts_table: Table = self._log_token_counts()

        # Service Panel
        title: str = (
            f"Document: {self.filename} - Segmentation and Preprocessing Results"
        )
        service_panel: Panel = Panel.fit(
            Columns(
                [sections_and_content_table, data_metrics_table, token_counts_table]
            ),
            title=title,
            title_align="left",
            border_style="red",
            padding=(1, 2),
        )

        richie(service_panel)

    def _segment_paragraphs(
        self,
    ) -> None:
        """Convert the JSON from the OCR-FR into a structured data model."""

        log.info(f"Preprocessing the document: {self.filename}")

        for index, paragraph in enumerate(
            self.analyse_result.paragraphs[: self.data_subset]
        ):
            # Process the text
            self._process_text(paragraph=paragraph)

            # Process the section heading
            self._process_section_heading(paragraph=paragraph)

            # Process the title
            self._process_title(paragraph=paragraph)

            # Check if we are at the end of the document
            self._document_end_check(index=index)

    def _segment_pages(
        self,
    ) -> None:
        """Convert the JSON from the OCR-FR into a structured data model."""

        log.info(f"Preprocessing the document: {self.filename}")

        pages_content: typing.List[processing_models.SegmentModel] = []
        for page in self.analyse_result.pages:
            page_number: int = page.page_number
            page_content = ""
            for line in page.lines:
                content: str = line.content
                page_content += content + " "

            content_model: processing_models.ContentModel = (
                processing_models.ContentModel(
                    text=page_content,
                )
            )

            segment_model: processing_models.SegmentModel = (
                processing_models.SegmentModel(
                    content=[content_model], title=f"{page_number}"
                )
            )

            pages_content.append(segment_model)

        self.segments = pages_content

    def _process_text(
        self,
        paragraph: processing_models.ParagraphModel,
    ) -> None:
        """
        Process the text in the paragraph.

        Args:
            paragraph (processing_models.ParagraphModel): The current paragraph.

        """

        if paragraph.role is None and paragraph.content is not None:
            text: str = paragraph.content.encode("ascii", "ignore").decode()
            # Check that the content is not None before appending the text
            if self.content is not None:
                self.content.set_text(text=f" {text}")
                [
                    self.content.bounding_regions.append(region)
                    for region in paragraph.bounding_regions
                    if region is not None
                ]  # Add the bounding regions to the current content

            # If the content is None, then this is a new section and we need to create a new content model
            elif self.content is None:
                self.content: processing_models.ContentModel = (
                    processing_models.ContentModel(
                        bounding_regions=paragraph.bounding_regions,
                    )
                )
                self.content.set_text(text=f" {text}")
                # Increment the content counter
                self.content_counter += 1

    def _process_section_heading(
        self,
        paragraph: processing_models.ParagraphModel,
    ) -> None:
        """
        Process the section heading in the paragraph.

        Args:
            paragraph (processing_models.ParagraphModel): The current paragraph.

        """

        # Setting the subheading
        if paragraph.role == configs.ParagraphSectionsConfig.SECTION_HEADING:

            # Check it isnt the first subheading, if content is None then we need to create a new content model
            if self.content is not None:
                # This is not the first subheading, so we need to add the current content to the section as we have a new subheading
                self.segment.content.append(self.content)

            # Create new content to hold the subheading and text
            self.current_subheading: str = paragraph.content
            self.content: processing_models.ContentModel = (
                processing_models.ContentModel(
                    section_heading=self.current_subheading,
                    # bounding_regions=paragraph.bounding_regions,
                )
            )

            self.content_counter += 1

    def _process_title(
        self,
        paragraph: processing_models.ParagraphModel,
    ) -> None:
        """
        Process the title in the paragraph.

        Args:
            paragraph (processing_models.ParagraphModel): The current paragraph.

        """

        if paragraph.role == configs.ParagraphSectionsConfig.TITLE:
            # Set the current title

            # Check to see if the current content is not non and the current content title is "N/A"
            if (
                self.content is not None
                and self.content.section_heading == self.default_section_heading
            ):
                # Append the current content to the section
                self.segment.content.append(self.content)

            # This is not the first section, so we need to add the current section to the result
            if self.segment is not None:
                self.segments.append(self.segment)

            # Create a new section
            self.current_title: str = paragraph.content
            self.segment: processing_models.SegmentModel = (
                processing_models.SegmentModel(
                    title=self.current_title,
                )
            )

            # Set the current subheading and content to None as we have a new section
            self.current_subheading = None
            self.content = None

            self.section_counter += 1

    def _document_end_check(
        self,
        index: int,
    ) -> None:
        """
        Check if we are at the end of the document and add the last section and content to the result.

        Args:
            index (int): The current index.
            data_subset (typing.List[processing_models.ParagraphModel]): The subset of the data.

        """

        if self.data_subset == -1:
            self.data_subset: int = len(self.analyse_result.paragraphs)

        # Check if we are at the end of the document
        if index == self.data_subset - 1:
            # Add the last content to the section
            if self.content is not None:
                self.segment.content.append(self.content)

            # Add the last section to the sections
            # Check to see if the section is not None
            if self.segment is not None:
                self.segments.append(self.segment)

    def _postprocess_segments(
        self,
    ) -> None:
        """
        Augment the segments by:
            - Removing empty content
            - Removing minimum content
            - Cleaning the text using GPT
            - Generating a heading using GPT
        """
        log.info(f"Cleaning {len(self.segments)} segments")

        indexes_to_remove: typing.Dict[int, typing.List[int]] = {}

        # Iterate through the segments
        for segment_index, segment in enumerate(self.segments):
            # Iterate through the content in the segment
            for content_index, content in enumerate(segment.content):

                # Empty content, we dont want to remove the content here as we are iterating through the content
                if self._check_empty_and_minimum_content(
                    content=content,
                    segment_index=segment_index,
                    content_index=content_index,
                    indexes_to_remove=indexes_to_remove,
                ):
                    continue

                self._clean_text(content=content)
                self._generate_heading(content=content)

        # Remove the content
        # We need to remove the content after we have iterated through the content
        self._remove_indexes(indexes_to_remove=indexes_to_remove)

    def _check_empty_and_minimum_content(
        self,
        content: processing_models.ContentModel,
        segment_index: int,
        content_index: int,
        indexes_to_remove: typing.Dict[int, typing.List[int]],
    ) -> bool:
        """
        Check if the content is empty or the minimum length.

        Args:
            content (processing_models.ContentModel): The content to check.
            segment_index (int): The segment index.
            content_index (int): The content index.
            indexes_to_remove (typing.Dict[int, typing.List[int]]): The indexes to remove.

        Returns:
            bool: True if the content is empty or the minimum length.

        """

        if content.text == "":
            # Check if the key exists in the dictionary
            if not indexes_to_remove.get(segment_index):
                indexes_to_remove[segment_index] = [content_index]
            else:
                indexes_to_remove[segment_index].append(content_index)
            return True

        elif len(content.text.split()) < self.minimum_sentence_length:

            if not indexes_to_remove.get(segment_index):
                indexes_to_remove[segment_index] = [content_index]
            else:
                indexes_to_remove[segment_index].append(content_index)
            return True

        return False

    def _remove_indexes(
        self,
        indexes_to_remove: typing.Dict[int, typing.List[int]],
    ):
        """Remove the indexes from the segments.

        Args:
            indexes_to_remove (typing.Dict[int, typing.List[int]]): The indexes to remove.

        """
        # Reverse the order of the indexes dictionary based on the keys
        indexes_to_remove_reverse: typing.Dict[int, typing.List[int]] = dict(
            reversed(indexes_to_remove.items())
        )

        # Iterate through the indexes
        for segment_index, content_indexes in indexes_to_remove_reverse.items():
            # Reverse the order of the content indexes
            content_indexes_reverse: typing.List[int] = list(reversed(content_indexes))

            # Iterate through the content indexes
            for content_index in content_indexes_reverse:
                self.segments[segment_index].content.pop(content_index)
                # Decrement the content counter
                self.content_counter -= 1

            # If the content is empty, remove the segment
            if len(self.segments[segment_index].content) == 0:
                self.segments.pop(segment_index)
                # Decrement the section counter
                self.section_counter -= 1

    def _generate_heading(
        self,
        content: processing_models.ContentModel,
    ) -> None:
        """
        Generate a heading from the text.

        Args:
            content (processing_models.ContentModel): The content to generate the heading from.

        """
        if content.section_heading == self.default_section_heading:
            # Generate the heading from the text
            prompt: str = """
                The task is to generate a heading from the text provided. The heading should be a short summary of the text.
                The text I will provide you with has been extracted from a document via OCR and is missing a heading.
                An example of a heading for the text 'Our audiences trust us to adhere to the highest editorial standards, and we have a right to freedom of expression, which is protected under the law.' could be 'The BBCs Editorial Values'.
                Please return the cleaned text as a single sentence, this can be capitalised or not, depending on what is appropriate.
            """

            conversation: typing.List[typing.Dict[str, str]] = [
                {
                    "role": "system",
                    "content": prompt,
                },
                {"role": "user", "content": content.text},
            ]

            response: ChatCompletion = self.chat_client.client.chat.completions.create(
                model=self.chat_model,
                messages=conversation,
            )

            # Set the heading
            content.section_heading: str = response.choices[0].message.content

    def _clean_text(
        self,
        content: processing_models.ContentModel,
    ) -> None:
        """
        Clean the text.

        Args:
            content (processing_models.ContentModel): The content to clean.

        """

        if content.text != "":
            prompt: str = """
                The task is to clean the text by removing the unwanted items. Do not remove any important information and do not add any information.
                The text I will provide you with has been extracted from a document via OCR. The assumption is that the text may contain some unwanted text like page numbers or random section headers. The text may not contain any errors in which case you should not make any changes and simply return the text. If the text only contains a few words and no errors then you should not make any changes and simply return the text.
                Here is an example: '2.3.2 These Editorial Guidelines are supplemented by further Guidance, which is available on the BBC Editorial Guidelines website.' the section heading '2.3.2' is not needed.
                Please return the cleaned text in the same format as that provided, i.e. a paragraph of text.
            """

            conversation: typing.List[typing.Dict[str, str]] = [
                {
                    "role": "system",
                    "content": prompt,
                },
                {"role": "user", "content": content.text},
            ]

            response: ChatCompletion = self.chat_client.client.chat.completions.create(
                model=self.chat_model,
                messages=conversation,
            )

            # Set the cleaned text
            content.text: str = response.choices[0].message.content

    def _log_sections_and_content(
        self,
    ):
        """Log the sections to the console."""

        # Create a table to display the results
        table: Table = create_table(
            title="Preprocessing Results",
            columns=["Section Count", "Content Count"],
            rows=[[str(self.section_counter), str(self.content_counter)]],
            caption="This table presents the preprocessing results for the document.",
        )

        return table

    def _log_token_counts(
        self,
    ) -> Table:
        """
        Log the token counts for the document.

        Returns:
            Table: The table to display the results.

        """

        max_token_count, average_token_count = self._calculate_token_count()

        # Create a table to display the results
        table: Table = create_table(
            title="Token Counts",
            columns=["Max Count", "Average Count"],
            rows=[[str(max_token_count), str(average_token_count)]],
            caption="This table presents the token counts for the document.",
        )

        return table

    def _calculate_token_count(
        self,
    ) -> int:
        """
        Calculate the token counts for the document.

        Returns:
            max_token_count (int): The max token count per paragraph.
            average_token_count (int): The average token count per paragraph.

        """

        token_counts: typing.List[int] = []

        # Iterate through the segments
        for segment in self.segments:
            # Iterate through the content in the segment
            for content in segment.content:
                token_counts.append(content.token_count)

        # Calculate the max token count
        max_token_count: int = max(token_counts)

        # Calculate the average token count
        average_token_count: int = sum(token_counts) / len(token_counts)

        return max_token_count, average_token_count
