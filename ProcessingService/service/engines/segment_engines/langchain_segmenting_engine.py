import typing

from loguru import logger as log
from companyloggers.richie import log as richie

from langchain_openai.embeddings import OpenAIEmbeddings
from langchain_experimental.text_splitter import SemanticChunker
from langchain.text_splitter import RecursiveCharacterTextSplitter

from companymodels import processing_models

import abstract


class LangchainSegmentingEngine(abstract.AbstractSegmenter):
    def __init__(
        self,
        segment_size: int = 1000,
        segment_overlap: int = 200,
    ):
        """
        Langchain segmenter class for segmenting documents.

        Recursive splitter:
        This is a recursive splitter that splits a text into segments of a given size, with a given overlap between each.
        This is returned as a list of strings, the strings are separated by a newline character.
        See: https://api.python.langchain.com/en/latest/text_splitter/langchain.text_splitter.TextSplitter.html

        Semantic splitter:
        This is a semantic chunker that splits a text into segments based on the meaning of the text.

        Args:
            chunk_size (int, optional): The size of each chunk. Defaults to 256.
            chunk_overlap (int, optional): The overlap between each chunk. Defaults to 0.

        """

        self.segment_size: int = segment_size

        self.recursive_splitter: RecursiveCharacterTextSplitter = (
            RecursiveCharacterTextSplitter(
                chunk_size=segment_size,
                chunk_overlap=segment_overlap,
            )
        )

        # self.semantic_splitter: SemanticChunker = SemanticChunker(OpenAIEmbeddings())

    def segment_data(
        self,
        filename: str,
        text_string: str,
    ) -> typing.List[processing_models.SegmentModel]:
        """
        Given a text string, return a list of SegmentModel`s using the Langchain.
        https://python.langchain.com/docs/modules/data_connection/document_transformers/text_splitters/recursive_text_splitter

        Args:
            filename (str): The filename of the document.
            text_string (str): The text string to segment.

        Returns:
            typing.List[processing_models.SegmentModel]: A list of `SegmentModel`s.

        """
        log.info(f"Segmenting data with segment size: {self.segment_size}.")

        text_split: typing.List[str] = self.recursive_splitter.split_text(
            text=text_string
        )
        content: typing.List[processing_models.ContentModel] = [
            processing_models.ContentModel(
                section_heading="",
                text=text,
            )
            for text in text_split
        ]

        segments: typing.List[processing_models.SegmentModel] = (
            processing_models.SegmentModel(
                title=filename,
                content=content,
            )
        )

        return segments

    # def semantic_segment(
    #     self,
    #     text_string: str,
    # ) -> typing.List[processing_models.SegmentModel]:
    #     """
    #     Given a text string using the Semantic Splitter method.
    #     https://python.langchain.com/docs/modules/data_connection/document_transformers/semantic-chunker
    #     https://github.com/FullStackRetrieval-com/RetrievalTutorials/blob/main/5_Levels_Of_Text_Splitting.ipynb

    #     Args:
    #         text_string (str): The text string to segment.

    #     """

    #     segments: typing.List = self.semantic_splitter.create_documents([text_string])
    #     log.debug(segments)
    #     return segments
