import typing

from rich.table import Table
from loguru import logger as log
from companyloggers.richie import create_table
from companyloggers.richie import log as richie

from companymodels import processing_models

import configs
import abstract

from ..utilities import SegmentingUtilities


class SegmentingPreprocessingToStringEngine(
    abstract.AbstractPreprocessor,
    SegmentingUtilities,
):
    """
    ToStringEngine converts the raw OCR-FR JSON data into a formatted text file with the following formatting:
        - Title: padded by triple newlines
        - Section heading: padded by double newlines
        - Text: padded by single newline
    """

    def __init__(
        self,
        filename: str,
        analyse_result: processing_models.DocumentAnalysisModel,
        prefix: bool = False,
        data_subset: int = -1,
    ):
        """
        PreprocessingEngine constructor.

        Args:
            filename (str): The filename of the document.
            analyse_result (processing_models.DocumentAnalyserEngine): The result of the OCR-FR analysis.
            prefix (bool, optional): Whether to prefix the title and section heading to the text. Defaults to False.
            data_subset (int, optional): The number of paragraphs to process. Defaults to -1.

        """

        # Initialise the parent class
        super().__init__(
            filename=filename,
            analyse_result=analyse_result,
        )

        self.filename: str = filename
        self.analyse_result: processing_models.DocumentAnalysisModel = analyse_result
        # Whether to prefix the title and section heading to the text, used for development purposes
        self.prefix: bool = prefix
        # Extract relevant data and generate formatted text or structured JSON
        self.data_subset: int = data_subset

        self.sections: str = ""  # Initialise sections
        self.current_title: str = None  # Initialise current title
        self.current_subheading: str = None  # Initialise current subheading

        # Counters
        self.section_counter: int = 0
        self.content_counter: int = 0

    def preprocess_data(
        self,
    ) -> None:
        """Convert the JSON from the OCR-FR into a string with a specific formatting."""

        # Extract relevant data and generate formatted text or structured JSON
        for paragraph in self.analyse_result.paragraphs[: self.data_subset]:
            # If the entry is a title, set the current title to the entry content
            self._set_title(paragraph=paragraph)

            # If the entry is a section heading, set the current subheading to the entry content
            self._set_subtitle(paragraph=paragraph)

            # If the entry is a paragraph, add the content to the result
            self._set_content(paragraph=paragraph)

        return self.sections

    def log_sections_and_content(
        self,
    ):
        """Log the sections to the console."""

        # Create a table to display the results
        table: Table = create_table(
            title="Preprocessing Results",
            columns=["Section Count", "Content Count"],
            rows=[[str(self.section_counter), str(self.content_counter)]],
        )

        richie(table)

    def _set_title(
        self,
        paragraph: processing_models.ParagraphModel,
    ) -> None:
        """
        Check if the entry is a title and if so, set the current title to the entry content.

        Args:
            paragraph (processing_models.Paragraph): The current paragraph.

        """

        prefix: str = "TITLE: " if self.prefix else ""

        if paragraph.role == configs.ParagraphSectionsConfig.TITLE:
            self.current_title: str = paragraph.content
            self.current_subheading = None
            self.sections += "\n\n" + f"{prefix}{self.current_title}" + "\n\n\n"

            # Increase the title counter
            self.section_counter += 1

    def _set_subtitle(
        self,
        paragraph: processing_models.ParagraphModel,
    ):
        """
        Check if the entry is a section heading and if so, set the current subheading to the entry content.

        Args:
            paragraph (processing_models.Paragraph): The current paragraph.

        """

        prefix: str = "SECTION HEADING: " if self.prefix else ""

        if paragraph.role == configs.ParagraphSectionsConfig.SECTION_HEADING:
            self.current_subheading: str = paragraph.content
            self.sections += "\n\n" + f"{prefix}{self.current_subheading}" + "\n\n"

            # Increase the content counter
            self.content_counter += 1

    def _set_content(
        self,
        paragraph: processing_models.ParagraphModel,
    ):
        """
        Check if the entry is a paragraph and if so, add the content to the result.

        Args:
            paragraph (processing_models.Paragraph): The current paragraph.

        """

        prefix: str = "TEXT: " if self.prefix else ""

        if paragraph.role is None and paragraph.content is not None:
            text: str = paragraph.content.encode("ascii", "ignore").decode()
            self.sections += f"{prefix}{text}" + "\n"
