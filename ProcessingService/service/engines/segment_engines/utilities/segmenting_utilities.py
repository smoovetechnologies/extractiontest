import typing

from rich.table import Table
from loguru import logger as log
from companyloggers.richie import create_table
from companyloggers.richie import log as richie

from companymodels import processing_models


class SegmentingUtilities:
    """
    Provides utilities for segmenting.
    Currently uncludes:

    - Calculate the average character count
    - Calculate the max character count
    - Calculate the average token count
    - Calculate the max token count
    """

    def __init__(
        self,
        filename: str,
        analyse_result: processing_models.DocumentAnalysisModel,
    ):
        """
        SegmentPreprocessingUtilities constructor.

        Args:
            filename (str): The filename of the document.
            analyse_result (processing_models.DocumentAnalysisModel): The result of the OCR-FR analysis.


        """
        self.filename: str = filename
        self.analyse_result: processing_models.DocumentAnalysisModel = analyse_result

        self.minimum_character_count: int = 2
        self.minimum_word_count: int = 2

        self.character_counts: typing.List[int] = []
        self.word_counts: typing.List[int] = []

    def calculate_data_metrics(self):
        """Calculate the data metrics for the document.

        Returns:
            typing.Dict[str, typing.Any]: The data metrics.

        """

        # Calculate the average character count
        average_character_count: int = self.calculate_average_character_count()

        # Calculate the max character count
        max_character_count: int = self.calculate_max_character_count()

        # Calculate the average token count
        average_token_count: int = self.calculate_average_token_count()

        # Calculate the max token count
        max_token_count: int = self.calculate_max_word_count()

        # Create the data metrics
        data_metrics: typing.Dict[str, typing.Any] = {
            "average_character_count": average_character_count,
            "max_character_count": max_character_count,
            "average_word_count": average_token_count,
            "max_word_count": max_token_count,
        }

        return data_metrics

    def calculate_average_character_count(self):
        """Calculate the average character count per paragraph.

        Returns:
            int: The average character count.

        """

        # Calculate the average character count
        if not self.character_counts:
            self._calculate_counts()

        # Average character count
        average_character_count: int = sum(self.character_counts) / len(
            self.character_counts
        )

        return average_character_count

    def calculate_max_character_count(self):
        """Calculate the average character per paragraph.

        Returns:
            int: The max character count.

        """

        # Calculate the average character count
        if not self.character_counts:
            self._calculate_counts()

        # Calculate the max character count
        max_character_count: int = max(self.character_counts)

        return max_character_count

    def calculate_average_token_count(self):
        """
        Calculate the average token count per paragraph.

        Returns:
            int: The average token count.

        """

        if not self.word_counts:
            self._calculate_counts()

        # Calculate the average token count
        average_token_count: int = sum(self.word_counts) / len(self.word_counts)

        return average_token_count

    def calculate_max_word_count(self):
        """
        Calculate the average paragraph size.

        Returns:
            int: The max token count per paragraph.

        """

        if not self.word_counts:
            self._calculate_counts()

        # Calculate the max token count
        max_token_count: int = max(self.word_counts)

        return max_token_count

    def log_data_metrics(self):
        """Log the data metrics for the document."""

        # Calculate the data metrics
        data_metrics: typing.Dict[str, typing.Any] = self.calculate_data_metrics()

        columns: typing.List[str] = list(data_metrics.keys())
        columns: typing.List[str] = [
            column.replace("_", " ").title() for column in columns
        ]

        rows: typing.List[str] = [str(value) for value in data_metrics.values()]

        # Log the data metrics
        data_metrics: Table = create_table(
            title=f"Paragraph Metrics: {self.filename}",
            columns=columns,
            rows=[rows],
            caption="This table presents the per paragraph metrics for the document.",
        )

        return data_metrics

    def _calculate_counts(self):
        """Calculate the character and word count in each paragraph."""

        # Define an empty list to store the character counts
        character_counts: typing.List[int] = []
        # Define an empty list to store the token counts
        word_counts: typing.List[int] = []

        # Loop through the paragraphs and get the character count
        for paragraph in self.analyse_result.paragraphs:
            if paragraph.role is None and paragraph.content is not None:
                character_counts.append(len(paragraph.content))
                word_counts.append(len(paragraph.content.split()))

        # Remove anything that is less than 2 characters
        self.character_counts: typing.List[int] = [
            character_count
            for character_count in character_counts
            if character_count > self.minimum_character_count
        ]
        # Remove anything that is less than 2 tokens
        self.word_counts: typing.List[int] = [
            word_count
            for word_count in word_counts
            if word_count > self.minimum_word_count
        ]
