import ast
import abc
import json
import typing

from loguru import logger as log

import abstract


class AbstractPromptParsingEngine(abc.ABC):
    @abc.abstractmethod
    def get_prompt(self) -> str:
        return NotImplementedError


class BaseParsingEngine(
    abstract.AbstractParser,
):
    def __init__(
        self,
        agent: abstract.AbstractAgent,
        prompt_controller: AbstractPromptParsingEngine,
    ) -> None:
        self.agent: abstract.AbstractAgent = agent

    def parse(
        self,
    ) -> typing.Dict[str, str]:

        try:
            response: str = self.agent.invoke_model(text=self.format_prompt())
            # Dict conversion
            response: typing.Dict = ast.literal_eval(response)
            return response
        except Exception as e:
            log.error(f"Error invoking model: {e}")

    def view_prompts(
        self,
        entities: typing.List[str],
        content: str,
    ) -> None:
        self.entities: typing.List[str] = entities
        self.content: str = content

        return (
            self.output_structure(),
            self.system_prompt(),
            self.format_prompt(),
        )

    def format_prompt(self) -> str:
        return self.system_prompt().format(content=self.content)

    def output_structure(self) -> str:
        entity_dictionary: typing.Dict[str, str] = {
            entity: entity.title().replace("_", " ")
            for entity in self.snake_case_entities()
        }

        return json.dumps(entity_dictionary, indent=4)

    def snake_case_entities(self) -> typing.List[str]:
        return [entity.lower().replace(" ", "_") for entity in self.entities]


class PromptParsingBaseEngine(AbstractPromptParsingEngine):
    def __init__(self, content: str, entities: typing.List[str]) -> None:
        self.content: str = content
        self.entities: typing.List[str] = entities

    def get_prompt(self) -> str:
        return self.format_prompt()

    def system_prompt(self) -> str:
        """A placeholder for the system prompt that should be implemented in the child class.

        Returns:
            str: The system prompt.

        """

        ...

    def view_prompts(
        self,
    ) -> typing.Tuple[str, str, str]:
        """
        View the prompts for the parsing engine.

        Returns:
            typing.Tuple[str, str, str]: The prompts for the parsing engine.

        """
        return (
            self.output_structure(),
            self.system_prompt(),
            self.format_prompt(),
        )

    def format_prompt(self) -> str:
        """Formats the prompt with the content.

        Returns:
            str: The formatted prompt.

        """
        return self.system_prompt().format(content=self.content)

    def output_structure(self) -> str:
        """Defines an output structure for the entities.
        The output string will be injected into the system prompt.

        Returns:
            str: A JSON string representation of the entities.

        """
        entity_dictionary: typing.Dict[str, str] = {
            entity: entity.title().replace("_", " ")
            for entity in self.snake_case_entities()
        }

        return json.dumps(entity_dictionary, indent=4)

    def snake_case_entities(self) -> typing.List[str]:
        """
        Converts the entities to snake case, important for the output structure as it will eventually be converted to a dictionary.

        Returns:
            typing.List[str]: The entities in snake case.

        """
        return [entity.lower().replace(" ", "_") for entity in self.entities]


class EntityParsingPromptEngine(BaseParsingEngine):

    def __init__(
        self,
        content: str,
        entities: typing.List[str],
    ):
        super().__init__(content=content, entities=entities)

    def system_prompt(self) -> str:
        return f"""
You are tasked with extracting the following entities from a given body of text: {', '.join(self.entities).lower()}.
If the entity is present, extract the entity in a format that can be casted to a Python dictionary.
Return this information only with no additional text, here is an example:

{{{self.output_structure()}}}

If the entity is not present return "Not Found" as the value of the entity.

Here is the text:
{{content}}
        """


class AddressParsingPromptEngine(BaseParsingEngine):
    def __init__(
        self,
        content: str,
        entities: typing.List[str],
    ):
        super().__init__(content=content, entities=entities)

    def system_prompt(self) -> str:

        return f"""
If the address is present, extract the address in a format that can be casted to a Python dictionary.
Return this information only with no additional text, here is an example:

{{{self.output_structure()}}}

If a field is not present return "Not Found" as the value of the field.

Here is the text:
{{content}}
        """
