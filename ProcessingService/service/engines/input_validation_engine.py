import typing

from companymodels import processing_models


class InputValidationEngine:
    def validate_segments(
        self,
        segments: typing.List[processing_models.SegmentModel],
    ):
        """
        Perform input validation on the input segments.

        Args:
            segments (typing.List[processing_models.SegmentModel]): List of segments.

        Raises:
            TypeError: If the segments parameter is not a list of SegmentModel.

        """

        # Input validation for the segments
        if (not isinstance(segments, typing.List)) or (
            not isinstance(segments[0], processing_models.SegmentModel)
        ):
            raise TypeError("The segments parameter must be a list of SegmentModel.")

    def validate_embedding_vectors(
        self,
        embedding_vectors: typing.List[processing_models.EmbeddingVectorModel],
    ):
        """
        Perform input validation on the input embedding vectors.

        Args:
            embedding_vectors (typing.List[processing_models.EmbeddingVectorModel]): List of embedding vectors.

        Raises:
            TypeError: If the embedding_vectors parameter is not a list of EmbeddingVectorModel.

        """

        # Input validation for the embedding vectors
        if (not isinstance(embedding_vectors, typing.List)) or (
            not isinstance(embedding_vectors[0], processing_models.EmbeddingVectorModel)
        ):
            raise TypeError(
                "The embedding_vectors parameter must be a list of EmbeddingVectorModel."
            )
