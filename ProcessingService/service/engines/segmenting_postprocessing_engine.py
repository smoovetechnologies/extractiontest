import typing

from companymodels import processing_models


class SegmentingPostprocessingEngine:
    def __init__(
        self,
        analyse_result: processing_models.DocumentAnalysisModel,
        text_segments: typing.List[processing_models.SegmentModel],
        embedding_vectors: typing.List[processing_models.EmbeddingVectorModel],
    ):
        """
        Constructor for the SegmentingPostProcessingEngine.

        Args:
            analyse_result ( processing_models.DocumentAnalysisModel): The result of the OCR analysis.
            text_segments (typing.List[ processing_models.SegmentModel]): The segments of the text.
            embedding_vectors (typing.List[ processing_models.EmbeddingVectorModel]): The embeddings of the segments.

        """
        self.analyse_result: processing_models.DocumentAnalysisModel = analyse_result
        self.text_segments: typing.List[processing_models.SegmentModel] = text_segments
        self.embedding_vectors: typing.List[processing_models.EmbeddingVectorModel] = (
            embedding_vectors
        )

    def postprocess(self):
        """Postprocess the embeddings."""
        self._map_bouding_regions()

    def _map_bouding_regions(self) -> None:
        """
        Map the bounding regions of the segmentas.
        Iterate over the text segments, embedding vectors, and OCR-FR result and map the sentences to each paragraph.
        This is done by checking to see if the sentence is in the paragraph, if it is then we extract the location
        data associated with the paragraph and add it to the SentenceModel object.
        """

        paragraphs: typing.List[processing_models.ParagraphModel] = (
            self.analyse_result.paragraphs
        )

        # Zip and iterate over the text segments and embedding vectors
        for segment, embedding in zip(self.text_segments, self.embedding_vectors):
            # Iterate over the paragraphs in the OCR-FR result
            for paragraph in paragraphs:
                # If the paragraph role field is None i.e. it is not a title or section heading
                # and the paragraph content field is not None i.e. it is a paragraph
                if paragraph.role is None and paragraph.content is not None:
                    # Check if the segment text is in the paragraph text if it isnt then continue to the next paragraph
                    if segment.text in paragraph.content:
                        # If it is then add the paragraph location data to the segment
                        embedding.bounding_regions = paragraph.bounding_regions
                        break
                    else:
                        continue
