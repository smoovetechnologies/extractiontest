import abc


class AbstractAgent(abc.ABC):
    @abc.abstractmethod
    def stream_chain(self) -> str:
        return NotImplementedError

    @abc.abstractmethod
    def invoke_chain(self) -> str:
        return NotImplementedError

    @abc.abstractmethod
    def stream_model(self) -> str:
        return NotImplementedError

    @abc.abstractmethod
    def invoke_model(self) -> str:
        return NotImplementedError


class AbstractParser(abc.ABC):
    @abc.abstractmethod
    def parse(self) -> str:
        return NotImplementedError
