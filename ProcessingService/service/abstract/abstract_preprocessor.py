import abc


class AbstractPreprocessor(abc.ABC):

    @abc.abstractmethod
    def preprocess_data(self):
        """Preprocess the data."""
        raise NotImplementedError
