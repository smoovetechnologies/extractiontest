import abc
import typing

from companymodels import processing_models


class AbstractSegmenter(abc.ABC):
    """Abstract class for segmenting a document into context segments."""

    @abc.abstractmethod
    def segment_data(self) -> typing.List[processing_models.SegmentModel]:
        """Segment the data and return a list of segments."""
        raise NotImplementedError
