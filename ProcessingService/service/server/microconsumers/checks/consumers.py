from rabbie import MicroConsumer

from companyconfigs import engines

import configs


# ---------------------------------------------------------- #


# MicroConsumer
consumer: MicroConsumer = MicroConsumer()

# Engines
log_engine: engines.LogEngine = engines.LogEngine(service_config=configs.service_config)


# ---------------------------------------------------------- #


@consumer.listen(
    queue=f"{configs.service_config.service_name}_{log_engine.health_check_queue.name}",
    workers=log_engine.health_check_queue.workers,
)
def health_check(body: str):
    """
    A simple health check consumer.

    Args:
        body (str): The body of the message.
    """

    log_engine.log_health_check()


# ---------------------------------------------------------- #


@consumer.listen(
    queue=f"{configs.service_config.service_name}_{log_engine.system_check_queue.name}",
    workers=log_engine.system_check_queue.workers,
)
def system_check(body: str):
    """
    A system check queue to understand how the system is configured.

    Args:
        body (str): The body of the message.
    """

    log_engine.log_system_check()


# ---------------------------------------------------------- #


@consumer.listen(
    queue=f"{configs.service_config.service_name}_{log_engine.service_check_queue.name}",
    workers=log_engine.service_check_queue.workers,
)
def service_check(body: str):
    """
    A service check endpoint to understand how the service is configured.

    Args:
        body (str): The body of the message.
    """

    log_engine.log_service_check(
        base_url=configs.base_url,
        environment=str(configs.system.environment),
        version=configs.system.version,
        life_cycle=configs.system.life_cycle,
    )
