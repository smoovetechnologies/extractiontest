import typing
import pathlib

from loguru import logger as log
from companyloggers.richie import log as richie

from fastapi import APIRouter, status
from fastapi.responses import JSONResponse
from azure.ai.formrecognizer import AnalyzeResult

from companymodels import processing_models
from companyutilities import io_utilities, endpoint_timer
from companypackout import DocumentIntelligenceAnnotatorEngine, AbstractAnnotator

import configs
import engines
import abstract
import initialised


# Router ---------------------------------------------------------- #

router: APIRouter = APIRouter(
    tags=["Document Annotation"],
    responses={404: {"description": "Page not found"}},
)

# Endpoints ---------------------------------------------------------- #

filename: str = "WaterBill"


@router.post(
    path="/annotate-paragraphs",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def annotate_paragraphs() -> JSONResponse:

    json_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.json"
    data: typing.Any = io_utilities.read_json(input_path=json_path)

    model_data: processing_models.DocumentAnalysisModel = (
        initialised.document_intelligence_loading_engine.load_from_data(data=data)
    )

    input_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.pdf"
    output_path: pathlib.Path = (
        configs.PathConfig.TEMPORARY_STORAGE / f"{filename}_Annotated_Paragraphs.pdf"
    )
    annotator: AbstractAnnotator = DocumentIntelligenceAnnotatorEngine(
        input_path=input_path,
        output_path=output_path,
        data=model_data,
        transposed=True,
    )
    annotator.annotate_paragraphs(data=model_data.paragraphs)
    annotator.save_annotations()


@router.post(
    path="/annotate-pages",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def annotate_pages() -> JSONResponse:

    json_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.json"
    data: typing.Any = io_utilities.read_json(input_path=json_path)

    model_data: processing_models.DocumentAnalysisModel = (
        initialised.document_intelligence_loading_engine.load_from_data(data=data)
    )

    input_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.pdf"
    output_path: pathlib.Path = (
        configs.PathConfig.TEMPORARY_STORAGE / f"{filename}_Annotated_Pages.pdf"
    )
    annotator: AbstractAnnotator = DocumentIntelligenceAnnotatorEngine(
        input_path=input_path,
        output_path=output_path,
        data=model_data,
        transposed=True,
    )
    annotator.annotate_pages(data=model_data.pages)
    annotator.save_annotations()


@router.post(
    path="/annotate-tables",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def annotate_tables() -> JSONResponse:
    filename: str = "MotorPolicy"
    json_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.json"
    data: typing.Any = io_utilities.read_json(input_path=json_path)

    model_data: processing_models.DocumentAnalysisModel = (
        initialised.document_intelligence_loading_engine.load_from_data(data=data)
    )

    input_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.pdf"
    output_path: pathlib.Path = (
        configs.PathConfig.TEMPORARY_STORAGE / f"{filename}_Annotated_Tables.pdf"
    )
    annotator: AbstractAnnotator = DocumentIntelligenceAnnotatorEngine(
        input_path=input_path,
        output_path=output_path,
        data=model_data,
        transposed=True,
    )
    annotator.annotate_tables(data=model_data.tables)
    annotator.save_annotations()
