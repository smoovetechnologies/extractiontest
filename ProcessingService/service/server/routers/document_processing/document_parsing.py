import json
import typing
import pathlib

from rich.table import Table
from loguru import logger as log
from companyloggers.richie import create_table
from companyloggers.richie import log as richie

import pydantic
import requests
from fastapi import APIRouter, status
from fastapi.responses import JSONResponse, Response

from companymodels import processing_models
from companyutilities import io_utilities, endpoint_timer
from companypackout import (
    DocumentIntelligenceEntityParsingEngine,
    AbstractDocumentIntelligenceEntityParsingEngine,
    DocumentIntelligenceAnnotatorEngine,
    AbstractAnnotator,
)

import configs
import engines
import abstract
import initialised


# Router ---------------------------------------------------------- #

router: APIRouter = APIRouter(
    tags=["Document Processing"],
    responses={404: {"description": "Page not found"}},
)


# Endpoints ---------------------------------------------------------- #

filename: str = "MotorPolicy"


@router.post(
    path="/get-troubleshooting-data",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def get_troubleshooting() -> JSONResponse:
    headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
    }
    json_data = {
        "query": "trouble shooting",
    }
    response = requests.post(
        "http://index_service:5003/index/search-redis-index",
        headers=headers,
        json=json_data,
    )
    response_data = response.json()[0]
    richie(response_data)
    # Read in the file
    filename = response_data["filename"]
    # Remove the extension
    filename = filename.split(".")[0]

    # Read in the json file
    json_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.json"
    data: typing.Any = io_utilities.read_json(input_path=json_path)

    # Find table 9 in the document
    page_number = response_data["title"]

    tables_data = data["tables"]

    for table in tables_data:
        if table["cells"][0]["bounding_regions"][0]["page_number"] == int(page_number):
            table_data = table

            break
    richie(table_data)

    content = {
        "page_number": response_data["title"],
        "content": response_data["text"],
        "table_structure": table_data,
    }

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=content,
    )


@router.post(
    path="/gpt-entity-extraction",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def gpt_entity_extraction() -> JSONResponse:

    json_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.json"
    data: typing.Any = io_utilities.read_json(input_path=json_path)
    content: str = data["content"]

    entities_to_extract: typing.List[str] = [
        "Customer Full Name",
        "Customer Full Address",
        "First Line of Address",
        "Customer Postcode",
        "CustomerCounty/Region",
        "Customer Town/City",
        "Reference Number",
        "Full Address of Property",
    ]

    prompts: typing.Tuple[str, str, str] = (
        initialised.entity_parsing_engine.view_prompts(
            entities=entities_to_extract,
            content=content,
        )
    )

    # Create a table
    table: Table = create_table(
        title="GPT Entity Extraction Prompts",
        caption="GPT Entity Extraction Prompts",
        columns=["Output Structure", "Format Prompt", "System Prompt"],
        rows=[prompts],
    )

    richie(table)

    parsed_entities: typing.Dict[str, str] = initialised.entity_parsing_engine.parse(
        content=content,
        entities=entities_to_extract,
    )

    # Write the entities to the cache
    initialised.redis_client.set("entities", json.dumps(parsed_entities))

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=parsed_entities,
    )


@router.post(
    path="/entity-metatadata-parsing",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def entity_metadata_parsing() -> JSONResponse:
    """Using the extracted entities, iterate through the document intelligence fields to extract the metadata."""

    json_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.json"
    data: typing.Any = io_utilities.read_json(input_path=json_path)

    # Load the entities from the cache
    entities: typing.Dict[str, str] = json.loads(
        initialised.redis_client.get("entities")
    )

    metadata_parser: AbstractDocumentIntelligenceEntityParsingEngine = (
        DocumentIntelligenceEntityParsingEngine(
            data=data,
            entities=entities,
            # create_index=True,
        )
    )

    entity_metadata: processing_models.ParsedContentModel = (
        metadata_parser.parse_pages()
    )
    # bouding_regions = metadata_parser.get_bounding_regions(
    #     augmented_entity_data=entity_metadata
    # )

    richie("entity_metadata", entity_metadata)
    # richie("bouding_regions", bouding_regions)

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={},
    )


@router.post(
    path="/entity-annotation",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def entity_annotation() -> JSONResponse:
    """Using the extracted entities, iterate through the pages and paragraphs fields to extract the metadata."""

    json_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.json"
    data: typing.Any = io_utilities.read_json(input_path=json_path)[0]
    entities = json.loads(initialised.redis_client.get("entities"))

    metadata_parser: AbstractDocumentIntelligenceEntityParsingEngine = (
        DocumentIntelligenceEntityParsingEngine(
            data=data,
            entities=entities,
            create_index=True,
        )
    )

    bounding_regions: typing.List[processing_models.BoundingRegionModel] = (
        metadata_parser.get_bounding_regions(
            augmented_entity_data=metadata_parser.parse_pages_and_paragraphs()
        )
    )

    input_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.pdf"
    output_path: pathlib.Path = (
        configs.PathConfig.TEMPORARY_STORAGE / f"{filename}_Annotated.pdf"
    )
    annotator: AbstractAnnotator = DocumentIntelligenceAnnotatorEngine(
        input_path=input_path,
        output_path=output_path,
        transposed=True,
    )

    annotator.annotate_bounding_regions(data=bounding_regions)
    annotator.save_annotations()

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={},
    )
