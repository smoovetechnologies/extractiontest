from .checks import router as check_router

from .document_processing import *
from .ocr import router as ocr_router
from .embedding import router as embedding_router
