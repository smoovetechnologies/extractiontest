import typing
import pathlib

from loguru import logger as log
from companyloggers.richie import log as richie

from fastapi import APIRouter, status
from fastapi.responses import Response
from fastapi import APIRouter, status, UploadFile, File

from companymodels import processing_models
from companyutilities import endpoint_timer, io_utilities
from companypackout import DocumentIntelligencePrebuiltModelsConfig

import configs
import initialised

# Router ---------------------------------------------------------- #
router: APIRouter = APIRouter(
    tags=["Embedding"],
    responses={404: {"description": "Page not found"}},
)


# Endpoints ---------------------------------------------------------- #


@router.post(
    path="/generate-embeddings-from-result",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def generate_embeddings_from_result(
    model_id: DocumentIntelligencePrebuiltModelsConfig,
    files: typing.List[UploadFile] = File(...),
) -> Response:
    """Generate the segments from the result of the OCR-FR."""

    for file in files:
        # Create a file object
        file_model: processing_models.FileModel = processing_models.FileModel(
            filename=file.filename,
            container_name="data",
            destination_path=configs.PathConfig.TEMPORARY_STORAGE / file.filename,
        )

        # Perform the OCR
        analyse_result: str = initialised.ocr_from_path_engine.run(
            model_id=model_id,
            file=file,
        )

        # Preprocess the data
        segments: typing.List[processing_models.SegmentModel] = (
            initialised.embedding_creation_pipeline.generate_segments_from_result(
                filename=file_model.filename,
                analyse_result=analyse_result,
            )
        )

        # Generate embeddings for each segment
        embeddings: typing.List[processing_models.EmbeddingVectorModel] = (
            initialised.embedding_creation_pipeline.generate_embeddings(
                file_data=file_model,
                segments=segments,
            )
        )

        # Remove the current embeddings from the database
        initialised.embedding_creation_pipeline.remove_current_embeddings(
            filename=file_model.filename,
        )
        # Push the embeddings to the database
        initialised.embedding_creation_pipeline.push_embeddings(
            filename=file_model.filename,
            embedding_vectors=embeddings,
        )

    return Response(
        status_code=status.HTTP_200_OK,
    )
