import json
import typing
import pathlib

from loguru import logger as log
from companyloggers.richie import log as richie

from fastapi import APIRouter, status
from fastapi.responses import JSONResponse
from azure.ai.formrecognizer import AnalyzeResult
from fastapi import APIRouter, status, UploadFile, File

from companymodels import processing_models
from companyutilities import endpoint_timer, io_utilities
from companypackout import DocumentIntelligencePrebuiltModelsConfig

import models
import engines
import configs
import abstract
import initialised


# Router ---------------------------------------------------------- #
router: APIRouter = APIRouter(
    tags=["OCR"],
    responses={404: {"description": "Page not found"}},
)

# Endpoints ---------------------------------------------------------- #


@router.post(
    path="/di-ocr-from-path",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def ocr_via_document_intelligence(
    model_id: DocumentIntelligencePrebuiltModelsConfig,
    files: typing.List[UploadFile] = File(...),
) -> JSONResponse:

    results: typing.List[str] = []
    # Run the OCR engine
    for file in files:
        result: str = initialised.ocr_from_path_engine.run(
            model_id=model_id, file=file, return_json=True
        )
        results.append(result)
        filename = file.filename
        filename = filename.split(".")[0]

        with open(
            configs.PathConfig.TEMPORARY_STORAGE / f"{filename}.json", "w"
        ) as file:
            file.write(result)

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=results,
    )


@router.post(
    path="/ocr-annotation",
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def ocr_annotation(
    model_id: DocumentIntelligencePrebuiltModelsConfig,
    files: typing.List[UploadFile] = File(...),
) -> JSONResponse:

    results: typing.List[str] = []
    for file in files:
        filename: str = file.filename
        result: AnalyzeResult = initialised.ocr_from_path_engine.run(
            model_id=model_id,
            file=file,
        )

        input_path: pathlib.Path = configs.PathConfig.TEMPORARY_STORAGE / filename
        output_path: pathlib.Path = (
            configs.PathConfig.TEMPORARY_STORAGE / f"Annotated_Pages_{filename}"
        )
        annotator = engines.DocumentIntelligenceAnnotatorEngine(
            input_path=input_path,
            output_path=output_path,
            data=results,
            transposed=True,
        )
        annotator.annotate_pages(data=result.pages)
        annotator.save_annotations()

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=results,
    )
