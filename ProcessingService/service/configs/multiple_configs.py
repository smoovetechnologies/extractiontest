import enum
import pathlib


import models


# Paths ---------------------------------------------------------- #
class DirectoryNameConfig:
    TEMPORARY_STORAGE: str = "temporary_storage"


class PathConfig:
    ROOT: pathlib.Path = pathlib.Path(__file__).parent.parent
    TEMPORARY_STORAGE: pathlib.Path = ROOT / DirectoryNameConfig.TEMPORARY_STORAGE


class AzureGPTDeploymentConfig:
    GPT35Turbo: models.AzureGPTInstanceModel = models.AzureGPTInstanceModel(
        name="GPT-3.5 Turbo",
        deployment_name="gpt35turbo",
        max_tokens=4096,
        streaming=True,
    )

    GPT4: models.AzureGPTInstanceModel = models.AzureGPTInstanceModel(
        name="GPT-4",
        deployment_name="GPT4",
        max_tokens=4096,
        streaming=True,
    )


# OCR ---------------------------------------------------------- #
class SupportedOCRTypeConig(str, enum.Enum):
    PDF: str = "application/pdf"
    PNG: str = "image/png"
    JPEG: str = "image/jpeg"


# Segmenting ---------------------------------------------------------- #
class SegmentGenerationMethodConfig(enum.Enum):
    """Enum class for segment generation methods."""

    RECURSIVE: str = "recursive"
    FIXED_SIZE: str = "fixed_size"


class ParagraphSectionsConfig:
    TITLE: str = "title"
    SECTION_HEADING: str = "sectionHeading"
    PARAGRAPH: str = "paragraph"


class EmbeddingConfig:
    EMBEDDING_GENERATION_TEXT_FORMAT: str = """
    {title}
    {section_heading}
    {text}
    """


if __name__ == "__main__":
    # Quick test of the config
    print(PathConfig.ROOT)
