from .service_config import (
    azure_document_intelligence_config,
    azure_ai_services_config,
    azure_gpt_api_credentials_config,
)


class EmbeddingCreationPipelineConfig:
    def __init__(
        self,
        document_intelligence_endpoint: str,
        document_intelligence_key: str,
        document_intelligence_model_id: str,
        embedding_model: str,
        chat_model: str,
        gpt_api_key: str,
        gpt_api_url: str,
        gpt_api_type: str,
        gpt_api_version: str,
    ):
        # Document analyser credentials for the document analyser engine
        self.document_intelligence_endpoint: str = document_intelligence_endpoint
        self.document_intelligence_key: str = document_intelligence_key

        # Document analyser model for the document analyser engine
        self.document_intelligence_model_id: str = document_intelligence_model_id

        # Embedding and chat models for the embedding engine
        self.embedding_model: str = embedding_model
        self.chat_model: str = chat_model

        # GPT API credentials for the GPT client
        self.gpt_api_key: str = gpt_api_key
        self.gpt_api_url: str = gpt_api_url
        self.gpt_api_type: str = gpt_api_type
        self.gpt_api_version: str = gpt_api_version

        # Segmenting generation method for the embedding engine
        # ! Deprecated
        # self.segmenting_generation_method: str = segmenting_generation_method


embedding_creation_pipeline_config: EmbeddingCreationPipelineConfig = (
    EmbeddingCreationPipelineConfig(
        document_intelligence_endpoint=azure_document_intelligence_config["ENDPOINT"],
        document_intelligence_key=azure_document_intelligence_config["KEY"],
        document_intelligence_model_id=azure_document_intelligence_config[
            "PREBUILT_MODEL_ID"
        ],
        embedding_model=azure_ai_services_config["embedding_model"],
        chat_model=azure_ai_services_config["chat_model"],
        gpt_api_key=azure_gpt_api_credentials_config["API_KEY"],
        gpt_api_url=azure_gpt_api_credentials_config["API_URL"],
        gpt_api_type=azure_gpt_api_credentials_config["API_TYPE"],
        gpt_api_version=azure_gpt_api_credentials_config["API_VERSION"],
    )
)
