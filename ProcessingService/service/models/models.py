import pydantic


class AzureGPTCredentialModel(pydantic.BaseModel):
    API_KEY: str
    API_URL: str
    API_TYPE: str
    API_VERSION: str


class AzureGPTInstanceModel(pydantic.BaseModel):
    name: str
    deployment_name: str
    streaming: bool
    max_tokens: int
