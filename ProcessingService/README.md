# Introduction

Here's a high-level flow of how the authentication process can works

The React client collects the user's username and password.
It sends a POST request to your FastAPI server's authentication endpoint, passing the credentials in the request body.
The FastAPI server validates the credentials against Firebase Authentication.
If the credentials are valid, the server generates a JWT token and returns it to the client.
The client stores the JWT token securely (e.g., in a secure HTTP cookie or local storage).
For subsequent requests that require authentication, the client includes the JWT token in the request headers.
The server verifies the JWT token for each authenticated request, ensuring that the request is coming from an authenticated user.
