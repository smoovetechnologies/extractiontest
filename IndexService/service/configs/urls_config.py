from companyconfigs import URLFormats
from companymodels import service_models

from .system_config import system
from .env_config import env_config


url_formats: service_models.URLFormatModel = service_models.URLFormatModel(
    client=URLFormats.CLIENT,
    service=URLFormats.SERVICE,
)

base_url: service_models.BaseURLModel = service_models.BaseURLModel(
    development=env_config.get("REACT_APP_DEVELOPMENT_URL"),
    staging=env_config.get("REACT_APP_STAGING_URL"),
    production=env_config.get("REACT_APP_PRODUCTION_URL"),
).get_base_url(environment=system.environment)
