import typing

from companyclients import keyvault_clients


dotenv_client: keyvault_clients.DotEnvClient = keyvault_clients.DotEnvClient()
env_config: typing.Dict = dotenv_client.get_env_vars()
