from companyconfigs import System

from .env_config import env_config

system: System = System(
    version=env_config.get("VERSION", ""),
    environment=env_config.get("REACT_APP_ENVIRONMENT", ""),
    life_cycle=env_config.get("LIFE_CYCLE", ""),
)
