import typing

from companyconfigs import base_classes
from companymodels import service_models

from .system_config import system
from .urls_config import base_url
from .env_config import env_config


class ServiceConfig(base_classes.BaseServiceConfig):
    def __init__(self, service_name: str):
        super().__init__(
            service_name=service_name,
            environment=system.environment,
            base_url=base_url,
            get_service_configuration_endpoint=env_config.get(
                "CONFIGURATION_SERVICE_GET_SERVICE_CONFIGURATION_GET_ENDPOINT"
            ),
            get_all_configurations_endpoint=env_config.get(
                "CONFIGURATION_SERVICE_GET_ALL_CONFIGURATIONS_GET_ENDPOINT"
            ),
            configuration_service_name=env_config.get("CONFIGURATION_SERVICE_NAME"),
            configuration_service_port=env_config.get("CONFIGURATION_SERVICE_PORT"),
            configuration_service_prefix=env_config.get("CONFIGURATION_SERVICE_PREFIX"),
            ping_endpoint=env_config.get("CONFIGURATION_SERVICE_PING_GET_ENDPOINT"),
        )


# Initialise the service configuration
config: ServiceConfig = ServiceConfig(service_name=env_config.get("INDEX_SERVICE_NAME"))

# Request the service configuration
config_dict: typing.Dict = config.request_configuration(
    configuration_service_url=config.configuration_service_url,
    configuration_service_endpoint=config.get_service_configuration_endpoint,
)

# Cast the service configuration
service_config: service_models.ServiceConfigModel = config.cast_service(
    service_configuration=config_dict
)

# Additional configurations
redis_index_connection_config: typing.Dict = service_config.additional_configs[
    "REDIS_INDEX_CONFIG"
]

# Azure database configurations
azure_cosmon_config: typing.Dict = service_config.additional_configs[
    "AZURE_COSMON_CONFIG"
]

# Azure AI configurations
azure_gpt_api_credentials_config: typing.Dict = service_config.additional_configs[
    "AZURE_GPT_CREDENTIALS_CONFIG"
]
azure_ai_services_config: typing.Dict = service_config.additional_configs[
    "AZURE_AI_SERVICES_CONFIG"
]
