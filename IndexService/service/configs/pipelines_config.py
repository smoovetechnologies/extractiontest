import typing

import configs

from .index_config import RedisIndexConfig, RedisIndexTypeConfig


class IndexEngineConfig:
    def __init__(
        self,
        index_name: str,
        index_type: RedisIndexTypeConfig,
        prefix: str,
        distance_metric: str,
        vector_field_name: str,
        fields: typing.List[str],
        embedding_model: str,
        chat_model: str,
        gpt_api_key: str,
        gpt_api_url: str,
        gpt_api_type: str,
        gpt_api_version: str,
    ):
        # Index configuration
        self.index_name: str = index_name
        self.index_type: RedisIndexTypeConfig = index_type
        self.prefix: str = prefix
        self.distance_metric: str = distance_metric
        self.vector_field_name: str = vector_field_name
        self.fields: typing.List[str] = fields

        # Embedding and chat models for the embedding engine
        self.embedding_model: str = embedding_model
        self.chat_model: str = chat_model

        # GPT API credentials for the GPT client
        self.gpt_api_key: str = gpt_api_key
        self.gpt_api_url: str = gpt_api_url
        self.gpt_api_type: str = gpt_api_type
        self.gpt_api_version: str = gpt_api_version


flat_redis_index_engine_config: IndexEngineConfig = IndexEngineConfig(
    index_name=RedisIndexConfig.INDEX_NAME,
    index_type=RedisIndexTypeConfig.FLAT,
    prefix=RedisIndexConfig.PREFIX,
    distance_metric=RedisIndexConfig.DISTANCE_METRIC,
    vector_field_name=RedisIndexConfig.VECTOR_FIELD_NAME,
    fields=RedisIndexConfig.get_fields(),
    embedding_model=configs.azure_ai_services_config["embedding_model"],
    chat_model=configs.azure_ai_services_config["chat_model"],
    gpt_api_key=configs.azure_gpt_api_credentials_config["API_KEY"],
    gpt_api_url=configs.azure_gpt_api_credentials_config["API_URL"],
    gpt_api_type=configs.azure_gpt_api_credentials_config["API_TYPE"],
    gpt_api_version=configs.azure_gpt_api_credentials_config["API_VERSION"],
)

hnsw_redis_index_engine_config: IndexEngineConfig = IndexEngineConfig(
    index_name=RedisIndexConfig.INDEX_NAME,
    index_type=RedisIndexTypeConfig.HNSW,
    prefix=RedisIndexConfig.PREFIX,
    distance_metric=RedisIndexConfig.DISTANCE_METRIC,
    vector_field_name=RedisIndexConfig.VECTOR_FIELD_NAME,
    fields=RedisIndexConfig.get_fields(),
    embedding_model=configs.azure_ai_services_config["embedding_model"],
    chat_model=configs.azure_ai_services_config["chat_model"],
    gpt_api_key=configs.azure_gpt_api_credentials_config["API_KEY"],
    gpt_api_url=configs.azure_gpt_api_credentials_config["API_URL"],
    gpt_api_type=configs.azure_gpt_api_credentials_config["API_TYPE"],
    gpt_api_version=configs.azure_gpt_api_credentials_config["API_VERSION"],
)
