import enum
import typing

from companymodels import processing_models


# ---------------------------------------------------------- #


# Redis
class RedisIndexConfig:
    VECTOR_FIELD_NAME: str = "embedding"  # Name of the field that stores the vectors
    INDEX_NAME: str = "embeddings-index"  # Name of the search index
    PREFIX: str = "doc"  # Prefix for the document keys
    DISTANCE_METRIC = "COSINE"  # Distance metric for the vectors (ex. COSINE, IP, L2)

    @classmethod
    def get_fields(cls) -> typing.List[str]:

        fields: typing.List[str] = [
            field for field in processing_models.EmbeddingVectorModel.__fields__.keys()
        ]

        exclusion_fields: typing.List[str] = ["vector_score", cls.VECTOR_FIELD_NAME]

        fields: typing.List[str] = [
            field for field in fields if field not in exclusion_fields
        ]

        return fields  # The fields to return in the search results and to store in the index (that are not the vector field)


class RedisIndexTypeConfig(enum.Enum):
    """
    The FLAT index is a simple index that stores vectors in a flat list.
    It is a good choice for small datasets where you want to run exact queries.
    The HNSW index is a graph-based index that uses a hierarchical navigable small world graph to store vectors.
    The HNSW index is a good choice for large datasets where you want to run approximate queries.

    Attributes:
        HNSW (str): The HNSW index type.
        FLAT (str): The FLAT index type.

    """

    HNSW: str = "HNSW"
    FLAT: str = "FLAT"


# ---------------------------------------------------------- #


class AnnoyIndexConfig:
    INDEX_DIMENSION: int = 1536


class AnnoyIndexMetricConfig(enum.Enum):
    ANGULAR: str = "angular"
    EUCLIDEAN: str = "euclidean"
    MANHATTAN: str = "manhattan"
    HAMMING: str = "hamming"
    DOT: str = "dot"
