import configs
import engines

from .initialised_clients import chat_client, embedding_client, redis_index_client

# Embedding engine
embedding_engine: engines.EmbeddingEngine = engines.EmbeddingEngine(
    embedding_client=embedding_client,
    chat_client=chat_client,
    embedding_model=configs.flat_redis_index_engine_config.embedding_model,
    chat_model=configs.flat_redis_index_engine_config.chat_model,
)

# Search engines
redis_seach_engine: engines.RedisSearchEngine = engines.RedisSearchEngine(
    index_name=configs.flat_redis_index_engine_config.index_name,
    vector_field_name=configs.flat_redis_index_engine_config.vector_field_name,
    return_fields=configs.flat_redis_index_engine_config.fields,
    redis_client=redis_index_client.client,
)
annoy_search_engine: engines.AnnoySearchEngine = engines.AnnoySearchEngine(
    nn=10,
)

# Log engine
log_engine: engines.LogEngine = engines.LogEngine()
