from companyclients import azure_clients, db_clients

import configs

# Azure
# Cosmon client
azure_cosmonru_connection_client: azure_clients.AzureCosmonConnectionClient = (
    azure_clients.AzureCosmonConnectionClient(
        connection_string=configs.azure_cosmon_config["CONNECTION_STRING"]
    )
)
azure_cosmonru_client: azure_clients.AzureCosmonClient = (
    azure_clients.AzureCosmonClient(
        client=azure_cosmonru_connection_client.client,
        database=configs.azure_cosmon_config["DATA"],
    )
)

# GPT Clients
# Chat
chat_client: azure_clients.AzureGPTClient = azure_clients.AzureGPTClient(
    api_key=configs.flat_redis_index_engine_config.gpt_api_key,
    api_url=configs.flat_redis_index_engine_config.gpt_api_url,
    api_type=configs.flat_redis_index_engine_config.gpt_api_type,
    api_version=configs.flat_redis_index_engine_config.gpt_api_version,
    deployment_name=configs.flat_redis_index_engine_config.chat_model,
)
# Embedding client
embedding_client: azure_clients.AzureGPTClient = azure_clients.AzureGPTClient(
    api_key=configs.flat_redis_index_engine_config.gpt_api_key,
    api_url=configs.flat_redis_index_engine_config.gpt_api_url,
    api_type=configs.flat_redis_index_engine_config.gpt_api_type,
    api_version=configs.flat_redis_index_engine_config.gpt_api_version,
    deployment_name=configs.flat_redis_index_engine_config.embedding_model,
)

# Redis
# This is not the cache it is the index
redis_index_client: db_clients.RedisClient = db_clients.RedisClient(
    host=configs.redis_index_connection_config["HOST"],
    port=configs.redis_index_connection_config["PORT"],
)
