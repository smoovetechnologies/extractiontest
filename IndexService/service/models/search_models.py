import pydantic


class SearchModel(pydantic.BaseModel):
    query: str
