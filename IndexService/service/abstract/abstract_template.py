import abc


class AbstractTemplate(abc.ABC):
    @abc.abstractmethod
    def get_template(self):
        pass
