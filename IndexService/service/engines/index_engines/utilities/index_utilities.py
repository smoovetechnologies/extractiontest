import typing

from loguru import logger as log

from companyclients import azure_clients

from companymodels import processing_models


class IndexUtilities:
    def __init__(
        self,
        collection_name: str,
        azure_cosmonru_client: azure_clients.AzureCosmonClient,
        embeddings: typing.List[processing_models.EmbeddingVectorModel] = None,
    ) -> None:
        """
        IndexCreationEngine constructor.

        Args:
            collection_name (str): The name of the collection in the database.
            embeddings (typing.List[processing_models.EmbeddingVectorModel], optional): The embeddings to use for the index. Defaults to None.

        """

        self.collection_name: str = collection_name
        self.azure_cosmonru_client: azure_clients.AzureCosmonClient = (
            azure_cosmonru_client
        )
        self.embeddings: typing.List[processing_models.EmbeddingVectorModel] = (
            embeddings
        )

    def get_embeddings(self) -> None:
        """Get the embeddings from the database if they are not already passed as an argument."""

        if self.embeddings is None:
            log.info("Getting embeddings from the database.")

            embeddings: typing.List[processing_models.EmbeddingVectorModel] = (
                self.azure_cosmonru_client.get_all_documents(
                    collection_name=self.collection_name
                )
            )

            # Cast the embeddings to a list of EmbeddingVectorModel
            self.embeddings: typing.List[processing_models.EmbeddingVectorModel] = [
                processing_models.EmbeddingVectorModel(**embedding)
                for embedding in embeddings
            ]
