import json
import typing

from rich.table import Table
from rich.panel import Panel
from rich.progress import track
from rich.columns import Columns
from loguru import logger as log
from companyloggers.richie import create_table
from companyloggers.richie import log as richie

import redis
import numpy as np
from redis.commands.search import field, indexDefinition
from redis.commands.search.field import TextField

from companymodels import processing_models
from companyclients import azure_clients, db_clients

import configs

from .utilities import IndexUtilities


class RedisIndexEngine(
    IndexUtilities,
):
    def __init__(
        self,
        prefix: str,
        index_name: str,
        distance_metric: str,
        vector_field_name: str,
        fields: typing.List[str],
        index_type: configs.RedisIndexTypeConfig,
        redis_client: redis.Redis,
        collection_name: str,
        cosmon_client: azure_clients.AzureCosmonClient,
        embeddings: typing.List[processing_models.EmbeddingVectorModel] = None,
    ) -> None:
        """
        IndexCreationEngine constructor.

        Args:

        """

        super().__init__(
            collection_name=collection_name,
            azure_cosmonru_client=cosmon_client,
            embeddings=embeddings,
        )

        # HNSW will take longer to build and consume more memory for most cases than FLAT but will be faster to run queries on, especially for large datasets.
        self.index_type: configs.RedisIndexTypeConfig = index_type  # Index type defines

        # Index parameters
        self.prefix: str = prefix  # The prefix for the document keys
        self.index_name: str = index_name  # The name of the index
        self.distance_metric: str = (
            distance_metric  # The distance metric to use i.e. how the vectors are compared
        )
        self.vector_field_name: str = (
            vector_field_name  # The name of the field that will be used to store the embeddings in the index
        )
        self.fields: typing.List[str] = (
            fields  # The fields to be indexed that are not the embeddings
        )

        # Clients
        self.collection_name: str = collection_name
        self.redis_client: db_clients.RedisClient.client = redis_client
        self.azure_cosmonru_client: azure_clients.AzureCosmonClient = cosmon_client

        # Its possible to pass the embeddings as an argument
        self.embeddings: typing.List[processing_models.EmbeddingVectorModel] = (
            embeddings
        )

    def derive_constants(
        self,
    ) -> None:
        """Derive the constants from the data."""
        log.info("Deriving constants from the data.")

        self.vector_dimenstion: int = len(
            self.embeddings[0].embedding
        )  # Length of the vectors

        self.vector_count: int = len(self.embeddings)  # Initial number of vectors

    def create_search_fields(self) -> None:
        """
        Define RediSearch fields for each of the entries in the dataset.
        These fields will be used to create the index. They will be derived from the embeddings schema.
        """

        log.info("Creating RediSearch fields for the index.")
        log.info(f"Embeddings are created under the name: {self.vector_field_name}")

        # Get the fields from the Models.EmbeddingVectorModel schema
        # Define RediSearch fields for each of the entries in the dataset

        text_fields: typing.List[TextField] = [
            field.TextField(
                name=field_name,
            )
            for field_name in self.fields
            if field_name != "vector_score"
        ]

        embedding_field: field.VectorField = field.VectorField(
            name=self.vector_field_name,
            algorithm=self.index_type.name,
            attributes={
                "TYPE": "FLOAT32",
                "DIM": self.vector_dimenstion,
                "DISTANCE_METRIC": self.distance_metric,
                "INITIAL_CAP": self.vector_count,
            },
        )

        self.all_index_fields: typing.List[field.Field] = text_fields + [
            embedding_field
        ]

    def create_index(self) -> None:
        """Create the index."""
        log.info("Creating the index.")

        # Check if index exists
        try:
            self.redis_client.ft(self.index_name).info()
            log.info("Index already exists.")
        except:
            log.info("Creating index.")
            # Create RediSearch Index
            self.redis_client.ft(self.index_name).create_index(
                fields=self.all_index_fields,
                definition=indexDefinition.IndexDefinition(
                    prefix=[self.prefix],
                    index_type=indexDefinition.IndexType.HASH,
                ),
            )

    def load_documents(self) -> None:
        """Load the documents into the index."""
        log.info("Loading documents into the index.")

        for embedding in self.embeddings:
            # Create a key for the document
            key: str = f"{self.prefix}:{str(embedding.id)}"

            content_embedding: bytes = np.array(
                object=embedding.embedding,
                dtype=np.float32,
            ).tobytes()

            # Convert the bouding regions to a string
            if embedding.bounding_regions is not None:
                bounding_regions: typing.List[typing.Dict] = [
                    bounding_region.dict()
                    for bounding_region in embedding.bounding_regions
                ]
                bounding_regions: str = json.dumps(bounding_regions)
            else:
                bounding_regions: str = ""

            # Set the new bounding regions
            embedding.bounding_regions = bounding_regions

            doc: typing.Dict[str, typing.Any] = {
                **embedding.dict(),
                self.vector_field_name: content_embedding,
            }

            self.redis_client.hset(key, mapping=doc)

        log.info(
            f"Loaded {self.redis_client.info()['db0']['keys']} documents, under the embedding name {self.vector_field_name}, in Redis search index with name {self.index_name}"
        )

    def log_index_information(self) -> None:
        """Log the index info."""
        log.info("Logging the index information.")

        index_information: typing.Dict = self.redis_client.ft(self.index_name).info()

        # Embedding metadata:
        # - Where the embeddings came from, i.e. the collection
        # - The embedding constants: vector dimension, vector count
        embedding_data_table: Table = create_table(
            title="Vector Embedding Data",
            columns=["Collection", "Vector Dimension", "Vector Count"],
            rows=[
                (
                    self.collection_name,
                    str(self.vector_dimenstion),
                    str(self.vector_count),
                )
            ],
            caption="This table presents the metadata for the vector embeddings.",
        )

        # Index metadata:
        # - The index name
        # - The key type
        # - The distance metric
        # - The prefix
        # - The index type
        index_metadata_table: Table = create_table(
            title="Index Metadata",
            columns=[
                "Index Name",
                "Distance Metric",
                "Prefix",
                "Index Type",
                "Key Type",
                "Loaded Documents",
            ],
            rows=[
                (
                    self.index_name,
                    self.distance_metric,
                    self.prefix,
                    str(self.index_type.name),
                    index_information["index_definition"][1],
                    index_information["num_docs"],
                )
            ],
            caption="This table presents the metadata for the RediSearch index.",
        )

        # Field metadata:
        # - The field names
        # - The field types
        fields_table: Table = create_table(
            title="Index Fields",
            columns=["Field", "Type"],
            rows=[
                (field.name, field.__class__.__name__)
                for field in self.all_index_fields
            ],
            caption="This table presents the fields that will be indexed in the RediSearch index.",
        )

        # Create a panel to display the tables
        title: str = f"Index Information - {self.index_name}"
        service_panel: Panel = Panel.fit(
            Columns([embedding_data_table, index_metadata_table, fields_table]),
            title=title,
            title_align="left",
            border_style="red",
        )

        richie(service_panel)
