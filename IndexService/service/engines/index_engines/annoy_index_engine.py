import typing

from rich.table import Table
from rich.panel import Panel
from rich.progress import track
from rich.columns import Columns
from loguru import logger as log
from companyloggers.richie import create_table
from companyloggers.richie import log as richie

from annoy import AnnoyIndex

from companyclients import azure_clients
from companymodels import processing_models

import configs

from .utilities import IndexUtilities


class AnnoyIndexEngine(
    IndexUtilities,
):
    """
    AnnoyIndexEngine class is used to create and search an index of vectors.

    Create an AnnoyIndex object, this is the search index
    Annoy (Approximate Nearest Neighbors Oh Yeah) library.
    Annoy is a C++ library with Python bindings used to search for points in space that are close to a given query point,
    more efficiently than brute force. It's particularly useful for tasks involving high-dimensional vectors, such as
    finding similar items in recommendation systems or similar images in a database.
    f=index_dim: This parameter specifies the number of dimensions for the index. In nearest neighbor search, data points are often represented in a multi-dimensional space, and this parameter defines the dimensionality of that space.
    metric=index_metric: This parameter specifies the distance metric used for measuring the similarity or distance between data points in the index. The choice of metric can affect the behavior of nearest neighbor searches.
    """

    def __init__(
        self,
        tree_number: int,
        index_metric: configs.AnnoyIndexMetricConfig,
        collection_name: str,
        cosmon_client: azure_clients.AzureCosmonClient,
        embeddings: typing.List[processing_models.EmbeddingVectorModel] = None,
    ):
        """
        Search engine requires data to be indexed, init creates an AnnoyIndex object.

        Args:
            tree_number (int): Number of trees to build the index with
            index_metric (configs.AnnoyIndexMetricConfig): Distance metric used for measuring the similarity or distance between data points in the index
            collection_name (str): The name of the collection in the database
            cosmon_client (azure_clients.AzureCosmonClient): The AzureCosmonClient used to interact with the database
            embeddings (typing.List[processing_models.EmbeddingVectorModel], optional): List of `EmbeddingVectorModel`s to be indexed. Defaults to None.

        """

        super().__init__(
            embeddings=embeddings,
            collection_name=collection_name,
            azure_cosmonru_client=cosmon_client,
        )

        # Index parameters
        self.index_metric: configs.AnnoyIndexMetricConfig = (
            index_metric  # Distance metric used for measuring the similarity or distance between data points in the index
        )
        self.tree_number: int = tree_number  # Number of trees to build the index with

        # Clients
        self.collection_name: str = (
            collection_name  # The name of the collection in the database
        )
        self.cosmon_client: azure_clients.AzureCosmonClient = (
            cosmon_client  # The AzureCosmonClient used to interact with the database
        )
        # Its possible to pass the embeddings as an argument
        self.embeddings: typing.List[processing_models.EmbeddingVectorModel] = (
            embeddings
        )

        self.segment_map: typing.Dict[int, str] = {}  # Map of index to segment

    def derive_constants(self) -> None:
        """Derive the constants from the data."""
        log.info("Deriving constants from the data.")

        self.vector_dimenstion: int = len(self.embeddings[0].embedding)
        self.vector_count: int = len(self.embeddings)

    def create_index(self) -> None:
        """Create the index."""
        log.info("Creating the index.")

        # Check if the index exists
        try:
            if self.index is not None:
                log.info("Index already exists.")
        except:
            log.info("Creating the index.")

            self.index: AnnoyIndex = AnnoyIndex(
                f=self.vector_dimenstion,
                metric=self.index_metric.name.lower(),
            )

    def load_documents(self) -> None:
        """Load the documents into the index."""
        log.info("Loading documents into the index.")

        for embedding in track(
            self.embeddings,
            description="Loading documents",
        ):
            # Populate index with index and embedding
            self.index.add_item(i=embedding.id, vector=embedding.embedding)

            # Load chunk text and indicies into map for retrieval
            self.segment_map[embedding.id] = embedding

        self.index.build(n_trees=self.tree_number)

        log.info(f"Loaded {len(self.embeddings)} documents into the Annoy index.")

    def log_index_information(self) -> None:
        """Log the index info."""
        log.info("Logging the index information.")

        # Embedding metadata:
        # - Where the embeddings came from, i.e. the collection
        # - The embedding constants: vector dimension, vector count
        embedding_data_table: Table = create_table(
            title="Vector Embedding Data",
            columns=["Collection", "Vector Dimension", "Vector Count"],
            rows=[
                (
                    self.collection_name,
                    str(self.vector_dimenstion),
                    str(self.vector_count),
                )
            ],
            caption="This table presents the metadata for the vector embeddings.",
        )

        # Index metadata:
        # - The index metric name
        # - The number of loaded documents
        index_metadata_table: Table = create_table(
            title="Index Metadata",
            columns=[
                "Index Metric",
                "Loaded Documents",
            ],
            rows=[
                (
                    self.index_metric.name,
                    str(self.vector_count),
                )
            ],
            caption="This table presents the metadata for the RediSearch index.",
        )

        # Create a panel to display the tables
        title: str = "Annoy Index Information"
        service_panel: Panel = Panel.fit(
            Columns([embedding_data_table, index_metadata_table]),
            title=title,
            title_align="left",
            border_style="red",
        )

        richie(service_panel)
