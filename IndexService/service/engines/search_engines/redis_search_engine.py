import typing

from loguru import logger as log

import redis
import numpy as np
from redis.commands.search import query, result


class RedisSearchEngine:
    def __init__(
        self,
        index_name: str,
        vector_field_name: str,
        return_fields: typing.List[str],
        redis_client: redis.Redis,
    ):
        """
        SearchEngine constructor.

        Args:
            redis_client (redis.Redis): The client for the Redis database.
            index_name (str): The name of the index to search.
            vector_field_name (str): The name of the vector field in the index.
            return_fields (typing.List[str]): The fields to return in the search results.

        """

        # This is the name of the field that will be used to store the score of the search results
        # This is how the search results will be sorted
        self.score: str = "vector_score"

        # The name that the index was created with
        self.index_name: str = index_name
        self.vector_field_name: str = vector_field_name
        self.return_fields: typing.List[str] = return_fields + [self.score]
        self.redis_client: redis.Redis = redis_client

    def search(
        self,
        query_embedding: typing.List[float],
        hybrid_fields="*",
        k: int = 5,
    ) -> typing.List[result.Document]:
        """
        Search the Redis index.

        Args:
            query_embedding (typing.List[float]): The embedding of the query.
            hybrid_fields (str, optional): Hybrid fields to search. Defaults to "*".
            k (int, optional): The number of results to return. Defaults to 5.

        Returns:
            typing.List[result.Document]: The search results.

        """
        log.info(f"Searching the Redis index: {self.index_name}")

        # Prepare the Query, this is the query that will be used to search the index using the KNN algorithm
        base_query: str = (
            f"{hybrid_fields}=>[KNN {k} @{self.vector_field_name} $vector as {self.score}]"
        )

        # Build a query obejct which uses the RediSearch Query Language
        redis_query: query.Query = (
            query.Query(base_query)
            .return_fields(*self.return_fields)
            .sort_by(self.score)
            .paging(0, k)
            .dialect(2)
        )

        # Prepare the Query Parameters
        query_parameters: typing.Dict[str, bytes] = {
            "vector": np.array(
                object=query_embedding,
                dtype=np.float32,
            ).tobytes()  # Convert the query embedding to a byte vector
        }

        results: result.Result = self.redis_client.ft(self.index_name).search(
            query=redis_query,
            query_params=query_parameters,
        )

        result_documents: typing.List[result.Document] = results.docs
        return result_documents

    def get_hybrid_field(
        self,
        field_name: str,
        value: str,
    ) -> str:
        """
        Create a hybrid field for the search.

        Read more here:
        https://cookbook.openai.com/examples/vector_databases/redis/getting-started-with-redis-and-openai#hybrid-queries-with-redis

        The previous examples showed how run vector search queries with RediSearch. In this section, we will show how to combine vector
        search with other RediSearch fields for hybrid search. In the below example, we will combine vector search with full text search.

        Example:
        Search the content vector for articles about famous battles in Scottish history and only include results with Scottish in the title

        Args:
            field_name (str): The name of the field to create.
            value (str): The value of the field to create.

        Returns:
            str: The hybrid field.

        """
        return f'@{field_name}:"{value}"'
