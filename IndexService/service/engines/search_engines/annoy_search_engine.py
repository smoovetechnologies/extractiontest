import typing

from loguru import logger as log

from annoy import (
    AnnoyIndex,
)  # This library is used for approximate nearest neighbour search

from companymodels import processing_models


class AnnoySearchEngine:
    """AnnoyIndexEngine class is used to search an index of vectors."""

    def __init__(
        self,
        nn: int,
    ):
        """
        AnnoySearchEngine constructor.

        Args:
            nn (int): Number of nearest neighbours to return

        """
        self.nn: int = nn

    # TODO: MUST BE A BETTER WAY TO DO THIS
    # dont want to pass in the index every time, rather have it as an instance variable
    def search(
        self,
        index: AnnoyIndex,
        segment_map: typing.Dict[int, str],
        query_embedding: typing.List[float],
    ) -> typing.List[processing_models.EmbeddingVectorModel]:
        """
        Search for the nearest neighbours to the query vector.

        Args:
            prompt_embedding (typing.List[float]): Query vector to search for

        Returns:
            typing.List[processing_models.EmbeddingVectorModel]: List of `EmbeddingVectorModel`s closest to the query vector

        """

        # Search for nearest neighbours
        search_vectors: typing.List[processing_models.EmbeddingVectorModel] = (
            self._nn_search(
                index=index,
                segment_map=segment_map,
                query_embedding=query_embedding,
            )
        )

        return search_vectors

    def _nn_search(
        self,
        index: AnnoyIndex,
        segment_map: typing.Dict[int, str],
        query_embedding: typing.List[float],
    ) -> typing.List[processing_models.EmbeddingVectorModel]:
        """
        Perform nearest neighbour search.

        Args:
            prompt_embedding (typing.List[float]): Query vector to search for
            nn (int): Number of nearest neighbours to return

        Returns:
            typing.List[processing_models.EmbeddingVectorModel]: List of `EmbeddingVectorModel`s closest to the query vector

        """

        indices: typing.List[int] = index.get_nns_by_vector(
            vector=query_embedding,
            n=self.nn,
        )

        embedding_vectors: typing.List[processing_models.EmbeddingVectorModel] = []
        for i in indices:
            embedding_vectors.append(segment_map[i])

        return embedding_vectors
