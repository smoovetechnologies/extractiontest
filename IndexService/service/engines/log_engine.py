import typing

from rich.table import Table
from rich.panel import Panel
from rich.columns import Columns
from loguru import logger as log
from companyloggers import create_table
from companyloggers.richie import log as richie

from redis.commands.search import result

from companymodels import processing_models

import configs


class LogEngine:
    def __init__(self): ...

    def log_redis_search_result(
        self,
        query: str,
        search_results: typing.List[result.Document],
        table_title: str,
    ):
        """
        Log the search results to the console.

        Args:
            query (str): The query that was used for the search.
            search_results (typing.List[result.Document]): The search results.
            table_title (str): The title of the table.

        """

        columns: typing.List[str] = [
            "Vector Score",
            "Filename",
            "Id",
            "Text",
            "Bounding Regions",
        ]

        rows: typing.List[typing.List[str]] = [
            [
                str(1 - float(document.vector_score)),
                document.filename,
                str(document.id),
                document.text,
                "Found bounds" if document.bounding_regions else "None",
            ]
            for document in search_results
        ]
        caption: str = f"Search Results for Query: {query}"

        table: Table = create_table(
            title=table_title,
            columns=columns,
            rows=rows,
            caption=caption,
        )

        richie(table)

    def log_annoy_search_result(
        self,
        query: str,
        search_results: typing.List[processing_models.EmbeddingVectorModel],
        table_title: str,
    ):
        """
        Log the search results to the console.

        Args:
            query (str): The query that was used for the search.
            search_results (typing.List[processing_models.EmbeddingVectorModel]): The search results.
            table_title (str): The title of the table.

        """
        log.info(f"Search Results for Query: {query}")

        table: Table = create_table(
            title=table_title,
            columns=["ID", "Filename", "Text"],
            rows=[
                (
                    str(document.id),
                    document.filename,
                    document.text,
                )
                for document in search_results
            ],
            caption=f"Search Results for Query: {query}",
        )

        richie(table)

    def log_index_information(
        self,
        index_information: typing.Dict,
        index_name: str,
        collection_name: str,
        vector_dimenstion: int,
        vector_count: int,
        distance_metric: str,
        prefix: str,
        index_type: str,
        fields: typing.List[str],
    ) -> None:
        """Log the index info."""
        log.info("Logging the index information.")

        # Embedding metadata:
        # - Where the embeddings came from, i.e. the collection
        # - The embedding constants: vector dimension, vector count
        embedding_data_table: Table = create_table(
            title="Vector Embedding Data",
            columns=["Collection", "Vector Dimension", "Vector Count"],
            rows=[
                (
                    collection_name,
                    str(vector_dimenstion),
                    str(vector_count),
                )
            ],
            caption="This table presents the metadata for the vector embeddings.",
        )

        # Index metadata:
        # - The index name
        # - The key type
        # - The distance metric
        # - The prefix
        # - The index type
        index_metadata_table: Table = create_table(
            title="Index Metadata",
            columns=[
                "Index Name",
                "Distance Metric",
                "Prefix",
                "Index Type",
                "Key Type",
                "Loaded Documents",
            ],
            rows=[
                (
                    index_name,
                    distance_metric,
                    prefix,
                    str(index_type),
                    index_information["index_definition"][1],
                    index_information["num_docs"],
                )
            ],
            caption="This table presents the metadata for the RediSearch index.",
        )

        # Field metadata:
        # - The field names
        # - The field types
        fields_table: Table = create_table(
            title="Index Fields",
            columns=["Field", "Type"],
            rows=[(field.name, field.__class__.__name__) for field in fields],
            caption="This table presents the fields that will be indexed in the RediSearch index.",
        )

        # Create a panel to display the tables
        title: str = f"Index Information - {self.index_name}"
        service_panel: Panel = Panel.fit(
            Columns([embedding_data_table, index_metadata_table, fields_table]),
            title=title,
            title_align="left",
            border_style="red",
        )

        richie(service_panel)
