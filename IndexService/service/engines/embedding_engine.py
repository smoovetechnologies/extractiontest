import typing

from loguru import logger as log

from openai.types.chat import ChatCompletion

from companyclients import azure_clients
from companymodels import processing_models


class EmbeddingEngine:
    """
    Class which converts segments to vector embeddings.

    The embeddings are generated using the OpenAI Embedding Model.

    OpenAI Embedding Model Documentation:
    https://platform.openai.com/docs/guides/embeddings

    old model: text-embedding-ada-002
    new small model: text-embedding-3-small
    new large model: text-embedding-3-large
    """

    def __init__(
        self,
        embedding_client: azure_clients.AzureGPTClient,
        chat_client: azure_clients.AzureGPTClient,
        embedding_model: str,
        chat_model: str,
    ) -> None:
        """
        Embedding engine constructor.

        Args:
            embedding_client (clients.AzureGPTClient): Embedding client is used to generate embeddings.
            chat_client (clients.AzureGPTClient): Chat client is used to summarise text if needed.
            embedding_model (str): The GPT model used to generate embeddings must be specified.
            chat_model (str): The GPT model used to summarise text must be specified.

        """

        self.embedding_client: azure_clients.AzureGPTClient = (
            embedding_client  # Embedding client is used to generate embeddings
        )
        self.chat_client: azure_clients.AzureGPTClient = (
            chat_client  # Chat client is used to summarise text if needed
        )
        self.chat_model: str = (
            chat_model  # The GPT model used to summarise text must be specified
        )
        self.embedding_model: str = (
            embedding_model  # The embedding model must be specified
        )

    def generate_embeddings(
        self,
        text: str,
    ) -> typing.List[float]:
        """
        Get the embeddings for the given text.
        Using the embedding client and the embedding model, generate the embeddings for the given text.

        Args:
            text (str): Text to generate embeddings for.

        Returns:
            typing.List[float]: List of embeddings.

        """

        embedding: typing.List[float] = (
            self.embedding_client.client.embeddings.create(
                input=text,
                model=self.embedding_model,
            )
            .data[0]
            .embedding
        )

        return embedding

    def summarise_text(self, text: str) -> str:
        """Given text, summarise it in no more than 250 words if it's more than 1750 characters (250-440 words)."""

        if len(text) <= 1750:
            return text

        conversation = [
            {
                "role": "system",
                "content": "Summarise the text the user inputs in around 250 words.",
            },
            {"role": "user", "content": text},
        ]

        response: ChatCompletion = self.chat_client.client.chat.completions.create(
            model=self.chat_model,
            messages=conversation,
        )

        response_text = response.choices[0].message.content

        return response_text

    def add_headings_to_text(
        self, chunk: processing_models.SegmentModel, text: str
    ) -> str:
        """Given some text, add the `Chunk`'s `title` and `section_heading`s to the text."""

        if not len(text):
            raise ValueError("`text` cannot be empty.")

        text_with_headings = ""

        if chunk.title:
            text_with_headings += chunk.title + "\n\n"

        if chunk.section_heading:
            text_with_headings += chunk.section_heading + "\n\n"

        text_with_headings += text

        return text_with_headings
