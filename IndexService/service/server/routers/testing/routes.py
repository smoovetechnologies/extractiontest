from loguru import logger as log

from fastapi import APIRouter, status
from fastapi.responses import Response

from companymodels import service_models
from companyutilities import endpoint_timer

import configs


# ---------------------------------------------------------- #


# FastAPI Router Instance
router: APIRouter = APIRouter(
    tags=["Testing"],
    responses={404: {"description": "Page not found"}},
)

# Endpoints
testing_endpooint: service_models.EndpointModel = configs.service_config.endpoints[
    "TESTING"
]


# ---------------------------------------------------------- #


@router.get(
    path=testing_endpooint.path,
    status_code=status.HTTP_200_OK,
)
@endpoint_timer
async def test_function() -> Response:
    """
    Sample function to test the service.

    Returns:
        JSONResponse: A response object with the health check response.
    """

    log.info("Hello World")
    return Response(
        status_code=testing_endpooint.status_code,
        content=testing_endpooint.response,
    )
