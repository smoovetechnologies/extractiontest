import json
import typing

from loguru import logger as log
from companyloggers.richie import log as richie

from fastapi import APIRouter, status
from redis.commands.search import result
from fastapi.responses import Response, Response

from companyutilities import endpoint_timer
from companymodels import processing_models

import models
import configs
import engines
import initialised

# ---------------------------------------------------------- #

# FastAPI Router Instance
router: APIRouter = APIRouter(
    # tags=["Redis Index Creation and Searching"],
    responses={404: {"description": "Page not found"}},
)


# ---------------------------------------------------------- #
# Index Creation


@router.get(
    path="/create-redis-flat-index",
    status_code=status.HTTP_200_OK,
    tags=["Redis Index Creation"],
)
@endpoint_timer
async def create_redis_flat_index() -> Response:
    """
    Create the FLAT Redis index.

    See more:
    https://cookbook.openai.com/examples/vector_databases/redis/getting-started-with-redis-and-openai#load-documents-into-the-index

    Returns:
        Response: A response object with the health check response.

    """

    index_engine: engines.RedisIndexEngine = engines.RedisIndexEngine(
        prefix=configs.flat_redis_index_engine_config.prefix,
        fields=configs.flat_redis_index_engine_config.fields,
        collection_name=configs.azure_cosmon_config["EMBEDDINGS"],
        redis_client=initialised.redis_index_client.client,
        cosmon_client=initialised.azure_cosmonru_client,
        index_type=configs.flat_redis_index_engine_config.index_type,
        index_name=configs.flat_redis_index_engine_config.index_name,
        distance_metric=configs.flat_redis_index_engine_config.distance_metric,
        vector_field_name=configs.flat_redis_index_engine_config.vector_field_name,
    )

    # Get the data from the data source
    index_engine.get_embeddings()

    # # Derive the constants from the data
    index_engine.derive_constants()

    # # Create the search fields
    index_engine.create_search_fields()

    # Create the index
    index_engine.create_index()

    # Load the documents into the index
    index_engine.load_documents()

    # Log the index
    index_engine.log_index_information()

    return Response(
        status_code=status.HTTP_200_OK,
    )


@router.get(
    path="/create-redis-hnsw-index",
    status_code=status.HTTP_200_OK,
    tags=["Redis Index Creation"],
)
@endpoint_timer
async def create_redis_hnsw_index() -> Response:
    """
    Create the HNSW Redis index.

    See more:
    https://cookbook.openai.com/examples/vector_databases/redis/getting-started-with-redis-and-openai#load-documents-into-the-index

    Returns:
        Response: A response object with the health check response.

    """

    index_engine: engines.RedisIndexEngine = engines.RedisIndexEngine(
        prefix=configs.hnsw_redis_index_engine_config.prefix,
        fields=configs.hnsw_redis_index_engine_config.fields,
        collection_name=configs.azure_cosmon_config["EMBEDDINGS"],
        redis_client=initialised.redis_index_client.client,
        cosmon_client=initialised.azure_cosmonru_client,
        index_type=configs.hnsw_redis_index_engine_config.index_type,
        index_name=configs.hnsw_redis_index_engine_config.index_name,
        distance_metric=configs.hnsw_redis_index_engine_config.distance_metric,
        vector_field_name=configs.hnsw_redis_index_engine_config.vector_field_name,
    )

    # Get the data from the data source
    index_engine.get_embeddings()

    # # Derive the constants from the data
    index_engine.derive_constants()

    # # Create the search fields
    index_engine.create_search_fields()

    # Create the index
    index_engine.create_index()

    # Load the documents into the index
    index_engine.load_documents()

    # Log the index
    index_engine.log_index_information()

    return Response(
        status_code=status.HTTP_200_OK,
    )


# ---------------------------------------------------------- #
# Index Searching


@router.get(
    path="/testing-redis-index-search",
    status_code=status.HTTP_200_OK,
    tags=["Redis Index Searching"],
)
@endpoint_timer
async def test_redis_index_search() -> Response:
    """
    Test the search against the Redis index.

    Returns:
        Response: A response object with the health check response.

    """

    query: str = "Troubleshooting"

    # Embed the query using the embedding engine
    query_embedding: typing.List[float] = (
        initialised.embedding_engine.generate_embeddings(text=query)
    )

    # Perform the search
    search_results: typing.List[result.Document] = (
        initialised.redis_seach_engine.search(query_embedding=query_embedding)
    )
    richie(search_results[0])
    initialised.log_engine.log_redis_search_result(
        query=query,
        search_results=search_results,
        table_title="Redis Search Results",
    )


@router.post(
    path="/search-redis-index",
    status_code=status.HTTP_200_OK,
    tags=["Redis Index Searching"],
)
@endpoint_timer
async def search_redis_index(
    body: models.SearchModel,
) -> Response:
    """
    Search the Redis index with the provided query.

    Returns:
        Response: A response object with the health check response.

    """
    query: str = body.query

    log.info("Searching the Redis index")

    # Embed the query using the embedding engine
    query_embedding: typing.List[float] = (
        initialised.embedding_engine.generate_embeddings(text=query)
    )

    # Perform the search
    search_results: typing.List[result.Document] = (
        initialised.redis_seach_engine.search(query_embedding=query_embedding)
    )

    initialised.log_engine.log_redis_search_result(
        query=query,
        search_results=search_results,
        table_title=f"Redis Search Results for \nQuery: [red]{query}[/red]",
    )

    # Cast the documents back to a custom model
    casted_search_results: typing.List[processing_models.EmbeddingVectorModel] = []
    for search_result in search_results:
        bounding_regions_model: typing.List[processing_models.BoundingRegionModel] = []

        if search_result.bounding_regions is not "":
            for bounding_region in json.loads(search_result.bounding_regions):
                region: processing_models.BoundingRegionModel = (
                    processing_models.BoundingRegionModel.from_dict(
                        bouding_region=bounding_region
                    )
                )
                bounding_regions_model.append(region)

        search_result_model: processing_models.EmbeddingVectorModel = (
            processing_models.EmbeddingVectorModel(
                id=int(search_result.id.split(":")[1]),
                creation_date=search_result.creation_date,
                last_modified_date=search_result.last_modified_date,
                title=search_result.title,
                section_heading=search_result.section_heading,
                word_count=search_result.word_count,
                token_count=search_result.token_count,
                filename=search_result.filename,
                embedding=[0.0],
                embedding_model=search_result.embedding_model,
                text=search_result.text,
                vector_score=search_result.vector_score,
                bounding_regions=bounding_regions_model,
            )
        )
        casted_search_results.append(search_result_model)

    content = [search_result.dict() for search_result in casted_search_results]

    return Response(
        status_code=status.HTTP_200_OK,
        content=json.dumps(content),
    )


@router.get(
    path="/hybrid-search-redis-index",
    status_code=status.HTTP_200_OK,
    tags=["Redis Index Searching"],
)
@endpoint_timer
async def hybrid_search_redis_index(
    query: str,
    field: str,
    value: str,
) -> Response:
    """
    Show how to combine vector search with other RediSearch fields for hybrid search

    Returns:
        Response: A response object with the health check response.

    """

    log.info(
        f"Hybird searching the Redis index with the query: {query}. Field: {field}. Value: {value}"
    )

    # Embed the query using the embedding engine
    query_embedding: typing.List[float] = (
        initialised.embedding_engine.generate_embeddings(text=query)
    )

    # Get hybird fields
    hybrid_fields: str = initialised.redis_seach_engine.get_hybrid_field(
        field_name=field,
        value=value,
    )

    # Perform the search
    search_results: typing.List[result.Document] = (
        initialised.redis_seach_engine.search(
            query_embedding=query_embedding,
            hybrid_fields=hybrid_fields,
        )
    )

    initialised.log_engine.log_redis_search_result(
        query=query,
        search_results=search_results,
        table_title="Redis Search Results",
    )
