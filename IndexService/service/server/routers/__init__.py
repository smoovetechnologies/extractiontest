from .checks import router as check_router
from .redis import router as redis_router
from .annoy import router as annoy_router


from .testing import router as testing_router
