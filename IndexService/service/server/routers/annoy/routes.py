import typing

from loguru import logger as log

from fastapi import APIRouter, status
from fastapi.responses import Response
from redis.commands.search import result

from annoy import AnnoyIndex

from companyutilities import endpoint_timer

import engines
import configs
import initialised

# ---------------------------------------------------------- #

# FastAPI Router Instance
router: APIRouter = APIRouter(
    # tags=["Annoy Index Creation and Searching"],
    responses={404: {"description": "Page not found"}},
)

# Engines
annoy_index: AnnoyIndex = None

# ---------------------------------------------------------- #


@router.get(
    path="/create-annoy-index",
    status_code=status.HTTP_200_OK,
    tags=["Annoy Index Creation"],
)
@endpoint_timer
async def create_annoy_index() -> Response:
    """
    Create the annoy index.

    Returns:
        Response: A response object with the health check response.

    """

    index_engine: engines.AnnoyIndexEngine = engines.AnnoyIndexEngine(
        index_metric=configs.AnnoyIndexMetricConfig.ANGULAR,
        tree_number=10,
        collection_name=configs.AzureCosmonCollections.EMBEDDINGS,
        cosmon_client=initialised.azure_cosmonru_client,
    )

    # Get the data from the data source
    index_engine.get_embeddings()

    # # Derive the constants from the data
    index_engine.derive_constants()

    # Create the index
    index_engine.create_index()

    # Load the documents into the index
    index_engine.load_documents()

    # Log the index
    index_engine.log_index_information()

    # Set the global variable
    global annoy_index
    annoy_index = index_engine

    return Response(
        status_code=status.HTTP_200_OK,
    )


@router.get(
    path="/testing-annoy-index-search",
    status_code=status.HTTP_200_OK,
    tags=["Annoy Index Searching"],
)
@endpoint_timer
async def test_annoy_index_search() -> Response:
    """
    Test the search against the Redis index.

    Returns:
        Response: A response object with the health check response.

    """

    query: str = "The Guidelines set the standards for the BBC."

    # Embed the query using the embedding engine
    query_embedding: typing.List[float] = (
        initialised.embedding_engine.generate_embeddings(text=query)
    )

    # Perform the search
    search_results: typing.List[result.Document] = (
        initialised.annoy_search_engine.search(
            index=annoy_index.index,
            segment_map=annoy_index.segment_map,
            query_embedding=query_embedding,
        )
    )

    initialised.log_engine.log_annoy_search_result(
        query=query,
        search_results=search_results,
        table_title="Redis Search Results",
    )
