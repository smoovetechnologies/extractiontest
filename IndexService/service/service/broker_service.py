from rabbie import MicroConsumer

from companywrappers import base_classes
from companymodels import wrapper_models

from server import microconsumers


class BrokerService(base_classes.BaseRabbitMQBrokerService):
    """Broker service class."""

    def __init__(
        self,
        service_name: str,
        host: str,
        port: int,
        username: str,
        password: str,
    ):
        """
        Broker service constructor.

        Args:
            service_name (str): The name of the service.
            host (str): The host of the broker.
            port (int): The port of the broker.
            username (str): The username of the broker.
            password (str): The password of the broker.
        """

        super().__init__(service_name=service_name)

        # Define the consumders
        consumers: list[MicroConsumer] = [
            router
            for router in vars(microconsumers).values()
            if isinstance(router, MicroConsumer)
        ]

        # Build the RabbitMQStarterPackModel
        broker_starter_pack: wrapper_models.RabbitMQStarterPackModel = (
            wrapper_models.RabbitMQStarterPackModel(
                service_name=service_name,
                host=host,
                port=port,
                username=username,
                password=password,
                consumers=consumers,
            )
        )

        # Create the broker
        self.create(broker_starter_pack=broker_starter_pack)
