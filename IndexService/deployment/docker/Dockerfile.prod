# Install the base image from Docker Hub
# Pull the base image from tiangolo (since we are using FastAPI to write the API) from Docker Hub
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.11

# ---------------------------------------------------------- #
# Install the packages from the requirements.txt file

# Upgrade pip and install requirements.
COPY ./requirements/requirements.txt /server/requirements.txt
RUN pip install --upgrade pip
RUN pip install -r /server/requirements.txt

# ---------------------------------------------------------- #
# Install the custom packages 

# These are the credentials for the private repository 
ARG PAT 
ARG USERNAME 
# Wrappers
RUN pip install companywrappers --index-url https://$USERNAME:$PAT@gitlab.com/api/v4/projects/52541173/packages/pypi/simple
# Logger 
RUN pip install companyloggers --index-url https://$USERNAME:$PAT@gitlab.com/api/v4/projects/52540883/packages/pypi/simple
# Utilities
RUN pip install companyutilities --index-url https://$USERNAME:$PAT@gitlab.com/api/v4/projects/52541063/packages/pypi/simple
# Config
RUN pip install companyconfigs --index-url https://$USERNAME:$PAT@gitlab.com/api/v4/projects/52540827/packages/pypi/simple
# Clients 
RUN pip install companyclients --index-url https://$USERNAME:$PAT@gitlab.com/api/v4/projects/52540688/packages/pypi/simple
# Models
RUN pip install companymodels --index-url https://$USERNAME:$PAT@gitlab.com/api/v4/projects/53679443/packages/pypi/simple

# ---------------------------------------------------------- #
# Complete the installation

# Clean the install
RUN apt-get -y clean

# Copy all files to /app directory and move into directory.
COPY ./service /server/service/

# Change the working directory 
WORKDIR /server/service

# Expose the application port to the host machine 
EXPOSE 80

# Run the fast application
CMD ["gunicorn", "-k", "uvicorn.workers.UvicornWorker", "--timeout", "1000", "--workers=2","-b", ":80","--log-level", "debug", "main:app"]
