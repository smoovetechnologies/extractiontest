# Introduction

This project is designed to provide a template service that can be used to quickly set up future projects while enforcing coding standards that make it easier for future developers to understand what was done for a given project.

This service can be operated as an API using FastAPI or a broker using RabbitMQ, this is configured in the main.py, the main entry point to the service.

The service consists of:

- The consistent folder structure used across the microservices.
- Prebuilt service classes to dynamically load exposed API routers and setup the API as well as expose the broker consumers and set up the service as a broker.
- Example routers/queues, including a health check, service check, system check.
- Example models, using the `dataclasses` package for standard dataclasses and `pydantic` for FastAPI models.
- `main.py` file with prebuilt logic to run the service.
- Prebuilt `Dockerfile` which will likely not need to be changed.
- Configured `ruff.toml` file for linting rules across Python code.
- Configured `mypy.ini` file for type checking rules across Python code.

## Folders

The service consists of the following folders:

- Documentation: for all documentation on the service.
- Deployment: hosts all Docker, Kubernetes, and Docker Swarm files.
- Requirements: the requirements file for production and development.
- Service: where the code for the service lives.
- Tests: Where all the tests for the service are written.

## API Service and Broker Service

This is a pre-structured, example of how we build an API service and broker consumer service. This sample contains an overall file structure, and some optional utility functionality that may make it quicker to deploy a service.

This service is built using [FastAPI](https://github.com/tiangolo/fastapi) and [Rabbie](https://github.com/scuffi/rabbie). They respectively enable the creation of a performant API/broker service quickly. Operating as an API creates a standard REST API allowing us to recieve HTTP requests. Operating as a message broker such as RabbitMQ over AMQP enables publishing and subscribing to queues to receive and send data.

# Setup

## Prerequisites

- Installed software:

  - Python 3.10.0
  - Docker
  - Git
  - "pipenv" pip package installed to Python 3.10 (can be installed via "pip install pipenv" on windows or "python3 -m pip install pipenv" on mac)
  - The service uses the Packages found here [Packages](https://dev.azure.com/CC-Data-Services/Packages). To install into a Docker container, pass the build args `PAT={PERSONAL_ACCESS_TOKEN_HERE}` when building.

An example of how build arguments are passed in Docker compose is:

```yaml
version: "3.9"

template_service:
  build:
    context: ../../../ROOT_DIRECTORY
    dockerfile: ./TemplateService/deployment/docker/Dockerfile.dev
    args:
      - PAT={PERSONAL_ACCESS_TOKEN_HERE} # <----- This is where you pass the arguments and/or token
  container_name: template-development
```

### Dev Packages

- pytest: used to run unit tests
- mypy: used to check typing
- jedi: used for auto completion in VSCode
- ruff: used for linting
- black: used for formatting

## Setup Steps

1. clone repo
2. open terminal within root directory of repository
3. run "pipenv install"
4. Check the virtual environment has been activated (the beginning of the line should have a (template_repo-xxxx) at the start of the line). if not then run "pipenv shell"
5. check pre-commit is installed by running: "pre-commit --version" within the terminal in the new virtual environment
6. If it is, run "pre-commit install" in the terminal. If not, try running "pipenv install pre-commit" before trying again.
7. Now when developing, if you try to "git commit" code that doesn't adhere to the coding standards we choose to enforce, the commit will be rejected and you will have to go fix them before commiting

### Running code using Docker

1. Ensure Docker Desktop is running
2. Within root directory run: "docker build -t template_image ."
3. Next run "docker run template_image"
4. the code will now run. Note: just a simple script for now.

### Running code using Virtual environment

1. If not already in virtual environment but have setup, run "pipenv shell" from root dir of repo.
2. from terminal run "cd template_app"
3. then run "python main.py --env "./config/envs/dev.env".
