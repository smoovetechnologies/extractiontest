from typing import Dict

from loguru import logger as log
from companyloggers.richie import log as richie

from fastapi import APIRouter, status
from fastapi.responses import JSONResponse, Response

from companymodels import service_models
from companyutilities import endpoint_timer

from configs import service_config, services


# ---------------------------------------------------------- #


# FastAPI Router Instance
router: APIRouter = APIRouter(
    tags=["Configuration Service"],
    responses={404: {"description": "Page not found"}},
)

# Endpoints
get_service_configuration_endpoint: service_models.EndpointModel = (
    service_config.endpoints["GET_SERVICE_CONFIGURATION"]
)
get_all_configurations_endpoint: service_models.EndpointModel = (
    service_config.endpoints["GET_ALL_CONFIGURATIONS"]
)
ping_endpoint: service_models.EndpointModel = service_config.endpoints["PING"]


# ---------------------------------------------------------- #


@router.get(
    path=get_service_configuration_endpoint.path,
    status_code=get_service_configuration_endpoint.status_code,
)
@endpoint_timer
async def get_service_configuration(service_name: str) -> JSONResponse:
    """
    Get the service configuration for the service name provided.
    This function will use the service name and index into the service mapper to find the correct service configuration.

    Args:
        service_name (str): The name of the service.

    Returns:
        JSONResponse: A response object with the health check response.
    """

    # log.info(f"{service_name} configuration requested")

    # Find the correct service configuration in the Services class.
    service: Dict = services.get_service(service_name=service_name)

    # richie(f"Found service: [red]{service_name}[/red]")

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=service,
    )


# ---------------------------------------------------------- #


@router.get(
    path=get_all_configurations_endpoint.path,
    status_code=get_all_configurations_endpoint.status_code,
)
@endpoint_timer
async def get_all_configurations() -> JSONResponse:
    """
    Get the service all service configurations.

    Returns:
        JSONResponse: A response object with the health check response.
    """

    # richie("All service configurations requested")

    # Find the correct service configuration in the Services class.
    all_configurations: str = services.get_all_services()

    # richie(f"Found [red]{len(all_configurations)}[/red] services")

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=all_configurations,
    )


# ---------------------------------------------------------- #


@router.get(
    path=ping_endpoint.path,
    status_code=ping_endpoint.status_code,
)
# @endpoint_timer
async def ping() -> Response:
    """
    Ping the service.

    Returns:
        Response: A response object with the health check response.
    """

    richie("[red]PING PONG![/red] Wow embarrassing...")

    return Response(
        status_code=status.HTTP_200_OK,
    )
