from fastapi import APIRouter
from fastapi.responses import JSONResponse

from companyconfigs import engines
from companyutilities import endpoint_timer

import configs


# ---------------------------------------------------------- #


# FastAPI Router Instance
router: APIRouter = APIRouter(
    tags=["Checks"],
    responses={404: {"description": "Page not found"}},
)

# engines
log_engine: engines.LogEngine = engines.LogEngine(service_config=configs.service_config)


# ---------------------------------------------------------- #


@router.get(
    path=log_engine.health_check_endpoint.path,
    status_code=log_engine.health_check_endpoint.status_code,
)
@endpoint_timer
async def health_check() -> JSONResponse:
    """
    A health check endpoint to understand how the service is configured.

    Returns:
        JSONResponse: A response object with the health check response.
    """

    log_engine.log_health_check()

    return JSONResponse(
        status_code=log_engine.health_check_endpoint.status_code,
        content=log_engine.health_check_endpoint.response,
    )


# ---------------------------------------------------------- #


@router.get(
    path=log_engine.system_check_endpoint.path,
    status_code=log_engine.service_check_endpoint.status_code,
)
@endpoint_timer
async def system_check() -> JSONResponse:
    """
    A system check endpoint to understand how the system is configured.

    Returns:
        JSONResponse: A response object with the health check response.
    """

    log_engine.log_system_check()

    return JSONResponse(
        status_code=log_engine.system_check_endpoint.status_code,
        content=log_engine.system_check_endpoint.response,
    )


# ---------------------------------------------------------- #


@router.get(
    path=log_engine.service_check_endpoint.path,
    status_code=log_engine.service_check_endpoint.status_code,
)
@endpoint_timer
async def service_check() -> JSONResponse:
    """
    A service check endpoint to understand how the service is configured.

    Returns:
        JSONResponse: A response object with the health check response.
    """

    log_engine.log_service_check(
        base_url=configs.base_url,
        environment=str(configs.system.environment),
        version=configs.system.version,
        life_cycle=configs.system.life_cycle,
    )

    return JSONResponse(
        status_code=log_engine.service_check_endpoint.status_code,
        content=configs.service_config.dict(),
    )
