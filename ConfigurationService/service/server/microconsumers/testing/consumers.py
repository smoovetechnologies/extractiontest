from rabbie import MicroConsumer
from loguru import logger as log

from companymodels import service_models

from configs import service_config


# ---------------------------------------------------------- #


# MicroConsumer
consumer: MicroConsumer = MicroConsumer()

# Endpoints
testing_queue: service_models.QueueModel = service_config.queues["TESTING"]


# ---------------------------------------------------------- #


@consumer.listen(
    queue=f"{service_config.service_name}_{testing_queue.name}",
    workers=testing_queue.workers,
)
def listen_one(body: str):
    """
    A sample function to test the service.

    Args:
        body (str): The message body.
    """

    log.info("Message received")
