from fastapi import FastAPI

import configs

from .api_service import APIService
from .broker_service import BrokerService


def setup_api(app: FastAPI) -> APIService:
    """
    Setup the api service.

    Args:
        app (FastAPI): The FastAPI app.

    Returns:
        APIService: The api service.

    """

    # Create a service object this will initialize the api wrapper class
    service: APIService = APIService(
        service_name=configs.service_config.service_name,
        service_port=configs.service_config.port,
        service_prefix=configs.service_config.prefix,
        app=app,
    )

    # This is outside guard clause to allow the api to be setup in both production and development
    # This will add the routers and middleware to the app
    service.setup()

    return service


def setup_broker() -> BrokerService:
    """
    Setup the broker service.

    Returns:
        BrokerService: The broker service.

    """

    # Read these in from env

    # This will initialise the broker wrapper class
    service: BrokerService = BrokerService(
        service_name=configs.service_config.service_name,
        host=configs.broker_config["HOST"],
        port=configs.broker_config["PORT"],
        username=configs.broker_config["USERNAME"],
        password=configs.broker_config["PASSWORD"],
    )

    # This is outside guard clause to allow the broker
    # to be setup in both production and development
    service.setup()

    return service
