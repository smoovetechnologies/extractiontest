from fastapi import APIRouter, FastAPI
from fastapi.middleware.cors import CORSMiddleware

from companymodels import wrapper_models
from companywrappers import base_classes

from server import routers


class APIService(base_classes.BaseFastAPIService):
    """API service class."""

    def __init__(
        self,
        service_name: str,
        service_port: int,
        service_prefix: str,
        app: FastAPI,
    ):
        """
        API service constructor.

        Args:
            service_name (str): The name of the service.
            service_port (int): The port to run the service on.
            service_prefix (str): The prefix to use for the service.
            app (FastAPI): The FastAPI instance to add the middleware to.

        """

        super().__init__(service_name=service_name)

        app: FastAPI = app

        # Add our custom middleware
        self._add_middleware(app)

        routes: list[APIRouter] = [
            router for router in vars(routers).values() if isinstance(router, APIRouter)
        ]

        # Build the FastAPIStarterPackModel
        api_starter_pack: wrapper_models.FastAPIStarterPackModel = (
            wrapper_models.FastAPIStarterPackModel(
                service_name=service_name,
                routers=routes,
                app=app,
                port=service_port,
                prefix=service_prefix,
            )
        )

        self.create(api_starter_pack=api_starter_pack)

    # Private methods
    def _add_middleware(self, app: FastAPI):
        """
        Adds the middleware to the app.

        Args:
            app (FastAPI): The FastAPI instance to add the middleware to.

        """

        # CORS middleware is elabored in the README.md file
        app.add_middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_methods=["*"],
            allow_headers=["*"],
            allow_credentials=True,
        )
