from enum import Enum, auto
import typing

from loguru import logger as log

from companymodels import service_models
from .utilities import JSONConfigUtilities
from .config import StartUpConfig, ConfigTypes
from .additional_configs import initialised_configs


class ConfigMapper(JSONConfigUtilities):
    def __init__(self, config_type: ConfigTypes):
        super().__init__()
        self.config_type: ConfigTypes = config_type

    def get_mappings(self, service_name: str) -> typing.Dict[str, typing.Any]:
        """Retrieve mappings for a service based on the config type."""
        if self.config_type == ConfigTypes.ADDITIONAL_CONFIGS:
            return self._get_additional_config_mappings(service_name)

        return self._merge_configurations(service_name)

    def _merge_configurations(self, service_name: str) -> typing.Dict[str, typing.Any]:
        """Merge default and service-specific configurations."""
        default_mapping = self._get_mappings(index=StartUpConfig.DEFAULT_CONFIG_INDEX)
        service_mapping = self._get_mappings(index=service_name)
        mappings = {**default_mapping, **service_mapping}
        log.success(
            f"{self.config_type.name.capitalize()} mappings for {service_name} retrieved."
        )
        return mappings

    def _get_mappings(self, index: str) -> typing.Dict[str, typing.Any]:
        """Get mappings from config."""
        service_config = self.get_config(
            index=index, config_type=self.config_type.name.lower()
        )
        return self._model_mapper(config=service_config)

    def _get_additional_config_mappings(
        self, service_name: str
    ) -> typing.Dict[str, typing.Any]:
        """Retrieve additional configurations for a service."""

        configs = self.get_config(
            index=service_name, config_type=self.config_type.name.lower()
        )
        result = {}
        for config in configs:
            if config in initialised_configs:
                result[config] = initialised_configs.get(config)
            else:
                log.error(f"Additional configuration {config} not found.")
                raise ValueError(f"Additional configuration {config} not found.")
        return result

    def _model_mapper(self, config: typing.Dict) -> typing.Dict[str, typing.Any]:
        """Map the configuration to the appropriate model."""
        model_cls = (
            service_models.EndpointModel
            if self.config_type == ConfigTypes.ENDPOINTS
            else service_models.QueueModel
        )
        return {key.upper(): model_cls(**value) for key, value in config.items()}


endpoints_config: ConfigMapper = ConfigMapper(ConfigTypes.ENDPOINTS)
queues_config: ConfigMapper = ConfigMapper(ConfigTypes.QUEUES)
additional_configs: ConfigMapper = ConfigMapper(ConfigTypes.ADDITIONAL_CONFIGS)
