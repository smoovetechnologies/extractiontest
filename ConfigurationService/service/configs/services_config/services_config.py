import typing

from loguru import logger as log

from companymodels import service_models

from ..utilities import EnvConfigUtilities
from ..urls_config import base_url, url_formats
from ..config_mapper import endpoints_config, queues_config, additional_configs


class Services(EnvConfigUtilities):
    """Service Definitions"""

    def __init__(self) -> None:
        """Service Definitions"""

        self.CONFIGURATION_SERVICE: service_models.ServiceConfigModel = (
            self._build_service_configuration(
                service_name="CONFIGURATION_SERVICE",
            )
        )

        # self.GENERATION_SERVICE: service_models.ServiceConfigModel = (
        #     self._build_service_configuration(
        #         service_name="GENERATION_SERVICE",
        #     )
        # )

        self.PROCESSING_SERVICE: service_models.ServiceConfigModel = (
            self._build_service_configuration(
                service_name="PROCESSING_SERVICE",
            )
        )

        self.INDEX_SERVICE: service_models.ServiceConfigModel = (
            self._build_service_configuration(
                service_name="INDEX_SERVICE",
            )
        )

    def get_service(self, service_name: str) -> typing.Optional[typing.Dict]:
        """
        Find the service in the service mapper.

        Args:
            service_name (str): The name of the service to find.

        Returns:
            typing.Optional[typing.Dict]: The service configuration as a dictionary.

        """

        # Get the service configuration
        service: service_models.ServiceConfigModel = getattr(
            self, service_name.upper(), None
        )

        # Return the service configuration as a dictionary
        if service:
            return service.dict()
        else:
            log.error(f"Service {service_name} not found.")
            return None

    def get_all_services(self) -> typing.Dict[str, typing.Dict]:
        """
        Get all services in the services.

        Returns:
            typing.Dict[str, Union[typing.Dict, typing.Dict]]: A dictionary of all services in the system.

        """

        # Get all attributes of in the init
        attributes: list[str] = dir(self)

        # Create a dictionary to store services
        services: typing.Dict[str, service_models.ServiceConfigModel] = {}

        # Filter attributes that are instances of ServiceConfigModel and add them to the dictionary
        for attr in attributes:
            if isinstance(getattr(self, attr), service_models.ServiceConfigModel):
                service = getattr(self, attr)
                services[service.service_name] = service.dict()

        return services

    # Private methods
    def _build_service_configuration(
        self,
        service_name: str,
    ) -> service_models.ServiceConfigModel:
        """
        Build the service configuration.

        Args:
            service_name (str): The name of the service.
            additional_configs (typing.Optional[typing.Dict], optional): Additional configurations for the service. Defaults to None.

        Returns:
             service_models.ServiceConfigModel: The service configuration.

        """

        service_config: service_models.ServiceConfigModel = (
            service_models.ServiceConfigModel(
                **self._get_container_configuration(service_name=service_name),
                base_url=base_url,
                url_formats=url_formats,
                endpoints=endpoints_config.get_mappings(service_name=service_name),
                queues=queues_config.get_mappings(service_name=service_name),
                additional_configs=additional_configs.get_mappings(
                    service_name=service_name
                ),
            )
        )

        return service_config

    def _get_container_configuration(self, service_name: str) -> typing.Dict:
        """
        Get the service container configuration based on the service name.

        Args:
            service_name (str): The name of the service.

        Returns:
            typing.Dict: The partial service configuration.

        """

        service_port: int = int(self.get_env_value(key=f"{service_name}_PORT"))
        service_prefix: str = self.get_env_value(key=f"{service_name}_PREFIX")
        service_container_name: str = self.get_env_value(
            key=f"{service_name}_CONTAINER_NAME"
        )
        service_name: str = self.get_env_value(key=f"{service_name}_NAME")

        partial_service_configuration: typing.Dict = {
            "service_name": service_name,
            "port": service_port,
            "prefix": service_prefix,
            "container_name": service_container_name,
        }

        return partial_service_configuration


services: Services = Services()
