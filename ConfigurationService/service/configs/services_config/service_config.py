import typing

from companyconfigs import base_classes
from companymodels import service_models

from ..system_config import system
from ..urls_config import base_url
from ..env_config import env_config
from .services_config import services


class ServiceConfig(base_classes.BaseServiceConfig):
    def __init__(self, service_name: str):
        super().__init__(
            service_name=service_name,
            environment=system.environment,
            base_url=base_url,
            get_service_configuration_endpoint=env_config.get(
                "CONFIGURATION_SERVICE_GET_SERVICE_CONFIGURATION_GET_ENDPOINT"
            ),
            get_all_configurations_endpoint=env_config.get(
                "CONFIGURATION_SERVICE_GET_ALL_CONFIGURATIONS_GET_ENDPOINT"
            ),
            configuration_service_name=env_config.get("CONFIGURATION_SERVICE_NAME"),
            configuration_service_port=env_config.get("CONFIGURATION_SERVICE_PORT"),
            configuration_service_prefix=env_config.get("CONFIGURATION_SERVICE_PREFIX"),
            ping_endpoint=env_config.get("CONFIGURATION_SERVICE_PING_GET_ENDPOINT"),
        )


# Initialise the service configuration
config: ServiceConfig = ServiceConfig(
    service_name=env_config.get("CONFIGURATION_SERVICE_NAME")
)

# Request the service configuration
config_dict: typing.Dict = services.get_service(
    service_name=env_config.get("CONFIGURATION_SERVICE_NAME")
)

service_config: service_models.ServiceConfigModel = config.cast_service(
    service_configuration=config_dict
)
