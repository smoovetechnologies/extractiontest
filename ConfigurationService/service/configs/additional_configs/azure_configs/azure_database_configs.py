from ...env_config import env_config


# ---------------------------------------------------------- #
# NoSQL Database


class AzureCosmosConnectionConfig:
    def __init__(self):
        self.URI: str = env_config.get("AZURE_COSMOS_DB_URL")
        self.KEY: str = env_config.get("AZURE_COSMOS_DB_PRIMARY_KEY")


class AzureCosmonConnectionConfig:
    def __init__(self):
        self.CONNECTION_STRING: str = env_config.get(
            "AZURE_COSMONRU_DB_CONNECTION_STRING"
        )


class AzureCosmonDatabases:
    def __init__(self):
        self.USERS: str = env_config.get("USERS_COSMOS_DATABASE_NAME")
        self.DATA: str = env_config.get("DATA_COSMOS_DATABASE_NAME")


class AzureCosmonCollections:
    def __init__(self):
        self.USER_PERSONAL_DATA: str = env_config.get(
            "USERS_PERSONAL_DATA_COSMOS_COLLECTION_NAME"
        )
        self.USER_STORAGE_REFERENCES: str = env_config.get(
            "USERS_STORAGE_REFERENCES_COSMOS_COLLECTION_NAME"
        )
        self.EMBEDDINGS: str = env_config.get("EMBEDDINGS_COSMOS_COLLECTION_NAME")
        self.OCR_RESULTS: str = "ocr_results"


class AzureCosmonConfig(
    AzureCosmonConnectionConfig,
    AzureCosmonDatabases,
    AzureCosmonCollections,
):
    def __init__(self):
        # Initialise all the inherited classes
        AzureCosmonConnectionConfig.__init__(self)
        AzureCosmonDatabases.__init__(self)
        AzureCosmonCollections.__init__(self)


# ---------------------------------------------------------- #
# Blob Storage


class AzureBlobConnectionConfig:
    def __init__(self):
        self.CONNECTION_STRING: str = env_config.get(
            "AZURE_BLOB_STORAGE_CONNECTION_STRING"
        )


class AzureBlobContainerConfig:
    def __init__(self):
        self.CONTAINER_NAME: str = "poc"
        self.OCR_RESULTS_CONTAINER_NAME: str = "ocrresults"


class AzureBlobConfig(
    AzureBlobConnectionConfig,
    AzureBlobContainerConfig,
):
    def __init__(self):
        # Initialise all the inherited classes
        AzureBlobConnectionConfig.__init__(self)
        AzureBlobContainerConfig.__init__(self)
