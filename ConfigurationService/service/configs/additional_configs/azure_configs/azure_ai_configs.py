from ...env_config import env_config


# ---------------------------------------------------------- #
# Document Intelligence


class AzureDocumentIntelligenceModels:
    """Document analyser models config."""

    def __init__(self):
        self.PREBUILT_MODEL_ID: str = "prebuilt-layout"


class AzureDocumentIntelligenceCredentials:
    """Document analyser credentials config."""

    def __init__(self):
        self.ENDPOINT: str = env_config["AZURE_FORM_RECOGNISER_ENDPOINT"]
        self.KEY: str = env_config["AZURE_FORM_RECOGNISER_KEY"]


class AzureDocumentIntelligenceConfig(
    AzureDocumentIntelligenceModels,
    AzureDocumentIntelligenceCredentials,
):
    """Document analyser config."""

    # Call the parent class constructors
    def __init__(self):
        # Initialise both parent classes
        AzureDocumentIntelligenceModels.__init__(self)
        AzureDocumentIntelligenceCredentials.__init__(self)


# ---------------------------------------------------------- #
# GPT


class GPTEmbeddingModels:
    """Azure Cognitive Services embedding models."""

    ADA_002: str = env_config["COGNITIVE_SERVICES_EMBEDDING_MODEL_ADA_002"]


class GPTChatModels:
    """Azure Cognitive Services language models."""

    GPT_35_TURBO: str = env_config["COGNITIVE_SERVICES_CHAT_MODEL_35_TURBO"]


class AzureGPTCredentialsConfig:
    """Azure GPT API credentials config."""

    def __init__(self):
        self.API_KEY: str = env_config["GPT_API_KEY"]
        self.API_URL: str = env_config["GPT_API_URL"]
        self.API_TYPE: str = env_config["GPT_API_TYPE"]
        self.API_VERSION: str = env_config["GPT_API_VERSION"]


# ---------------------------------------------------------- #
# AI Services


class AzureAIServicesCredentialsConfig:
    """Azure Cognitive Services credentials config."""

    def __init__(self):
        self.API_KEY: str = env_config["COGNITIVE_SERVICES_API_KEY"]
        self.API_URL: str = env_config["COGNITIVE_SERVICES_URL"]


class AzureAIServicesConfig(
    AzureAIServicesCredentialsConfig,
):
    """Azure Cognitive Services config."""

    def __init__(
        self,
        embedding_model: str,
        chat_model: str,
    ):
        super().__init__()

        self.embedding_model: str = embedding_model
        self.chat_model: str = chat_model
