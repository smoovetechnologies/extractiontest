from ..env_config import env_config


class RedisCacheConfig:
    def __init__(self):
        self.HOST: str = env_config.get("CACHE_SERVICE_NAME")
        self.PORT: int = env_config.get("CACHE_SERVICE_PORT")


class RedisIndexConfig:
    def __init__(self):
        self.HOST: str = env_config.get("REDIS_INDEX_NAME")
        self.PORT: int = env_config.get("REDIS_INDEX_PORT")
