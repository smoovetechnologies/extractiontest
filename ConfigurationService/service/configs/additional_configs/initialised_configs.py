import typing

from loguru import logger as log

from .redis_config import RedisCacheConfig, RedisIndexConfig
from .azure_configs import AzureCosmonConfig, AzureBlobConfig
from .azure_configs import (
    AzureDocumentIntelligenceConfig,
    AzureGPTCredentialsConfig,
    AzureAIServicesConfig,
    GPTEmbeddingModels,
    GPTChatModels,
)
from .multiple_configs import (
    DocumentIntelligencePrebuiltModelsConfig,
    SupportedOCRTypeConig,
)
from ..utilities import key_builder

# Initialised configurations
# Services

redis_cache_config: RedisCacheConfig = RedisCacheConfig()
redis_index_config: RedisIndexConfig = RedisIndexConfig()

# Azure
azure_cosmon_config: AzureCosmonConfig = AzureCosmonConfig()
azure_blob_config: AzureBlobConfig = AzureBlobConfig()
azure_document_intelligence_config: AzureDocumentIntelligenceConfig = (
    AzureDocumentIntelligenceConfig()
)
azure_gpt_credentials_config: AzureGPTCredentialsConfig = AzureGPTCredentialsConfig()

azure_ai_services_config: AzureAIServicesConfig = AzureAIServicesConfig(
    embedding_model=GPTEmbeddingModels.ADA_002,
    chat_model=GPTChatModels.GPT_35_TURBO,
)

document_intelligence_prebuilt_models_config: (
    DocumentIntelligencePrebuiltModelsConfig
) = DocumentIntelligencePrebuiltModelsConfig
supported_ocr_type_config: SupportedOCRTypeConig = SupportedOCRTypeConig

microservice_connection_configs: typing.Dict[str, typing.Dict] = {
    key_builder(redis_cache_config.__class__.__name__): redis_cache_config.__dict__,
    key_builder(redis_index_config.__class__.__name__): redis_index_config.__dict__,
}

ocr_configs: typing.Dict[str, typing.Dict] = {
    key_builder(
        document_intelligence_prebuilt_models_config.__class__.__name__
    ): document_intelligence_prebuilt_models_config.__dict__,
    key_builder(
        supported_ocr_type_config.__class__.__name__
    ): supported_ocr_type_config.__dict__,
}

azure_configs: typing.Dict[str, typing.Dict] = {
    key_builder(azure_cosmon_config.__class__.__name__): azure_cosmon_config.__dict__,
    key_builder(azure_blob_config.__class__.__name__): azure_blob_config.__dict__,
    key_builder(
        azure_document_intelligence_config.__class__.__name__
    ): azure_document_intelligence_config.__dict__,
    key_builder(
        azure_gpt_credentials_config.__class__.__name__
    ): azure_gpt_credentials_config.__dict__,
    key_builder(
        azure_ai_services_config.__class__.__name__
    ): azure_ai_services_config.__dict__,
}

initialised_configs: typing.Dict[str, typing.Dict] = {
    **microservice_connection_configs,
    **azure_configs,
    **ocr_configs,
}
