import enum


class SupportedOCRTypeConig(str, enum.Enum):
    def __init__(self):
        self.PDF: str = "application/pdf"
        self.PNG: str = "image/png"
        self.JPEG: str = "image/jpeg"


class DocumentIntelligencePrebuiltModelsConfig(str, enum.Enum):
    def __init__(self):
        self.READ: str = "prebuilt-read"
        self.GENERAL_DOCUMENT: str = "prebuilt-document"
        self.LAYOUT: str = "prebuilt-layout"

        self.RECEIPT: str = "prebuilt-receipt"
        self.INVOICE: str = "prebuilt-invoice"
        self.IDENTITY_DOCUMENT: str = "prebuilt-idDocument"
