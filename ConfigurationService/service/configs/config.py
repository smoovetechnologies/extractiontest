import enum


class StartUpConfig:
    JSON_CONFIG_FILENAME: str = "config.json"
    DEFAULT_CONFIG_INDEX: str = "DEFAULT_CONFIG"


class ConfigTypes(enum.Enum):
    ENDPOINTS = enum.auto()
    QUEUES = enum.auto()
    ADDITIONAL_CONFIGS = enum.auto()
