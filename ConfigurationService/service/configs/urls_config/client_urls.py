from .urls_config import base_url, url_formats


class ClientURLs:
    """Defines the urls that will be used by the client."""

    def __init__(self) -> None:
        """Initialize the ClientURLs class.
        These are in the init because they are converted to a __dict__ to be JSON serializable and passed to any service that needs them.
        """

        # Client Service
        url_format: str = url_formats.client


client_urls: ClientURLs = ClientURLs()
