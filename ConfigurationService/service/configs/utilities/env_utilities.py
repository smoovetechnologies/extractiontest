import typing

from loguru import logger as log

from configs.env_config import env_config


class EnvConfigUtilities:
    """Configuration utilities functions to aid in the process of setting up services"""

    def __init__(self):
        """Configuration utilities constructor."""
        pass

    def get_env_value(self, key: str) -> typing.Optional[typing.Union[str, int]]:
        """
        Get the env value from the environment variables.

        Args:
            key (str): The key to get the value for.

        Returns:
            typing.Optional[typing.Union[str, int]]: The value of the key.

        """

        value: typing.Optional[typing.Union[str, int]] = env_config.get(key)

        if not value:
            log.warning(
                f"{key} not found in environment variables, prefixing value with REACT_APP."
            )

        return value if value is not None else self.get_prefix(key)

    def get_prefix(self, key: str) -> typing.Optional[typing.Union[str, int]]:
        """Get the prefix for the service.

        Args:
            key (str): The key of the environment variable.

        Returns:
            typing.Optional[str]: The environment variable.

        Raises:
            ValueError: If the key is not found.

        """

        prefixed_key: str = f"REACT_APP_{key}"

        value: typing.Optional[typing.Union[str, int]] = env_config.get(prefixed_key)

        if not value:
            log.error(
                f"{key} not found. Please check the environment variables. Prefixing value with REACT_APP did not work."
            )
            raise ValueError(f"{key} not found in environment variables.")

        return value
