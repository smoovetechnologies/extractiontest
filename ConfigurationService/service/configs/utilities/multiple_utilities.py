import re
import typing


def key_builder(config_class_name: str) -> str:
    # This regex matches transitions from lowercase to uppercase letters and
    # treats sequences of uppercase letters followed by a lowercase letter as a single block.
    parts: typing.List[str] = re.findall(
        r"[A-Z]+(?=[A-Z][a-z])|[A-Z]?[a-z]+|[A-Z]+(?=[A-Z]|\b)", config_class_name
    )
    new_name: str = "_".join(parts).upper()
    return new_name
