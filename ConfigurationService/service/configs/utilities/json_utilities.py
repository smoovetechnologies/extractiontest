import json
import typing
import pathlib

from loguru import logger as log

from ..config import StartUpConfig


class JSONConfigUtilities:
    def __init__(self) -> None:
        """Config Utilities constructor."""

        self.json_config: typing.Dict = self._load_json_config(
            filename=StartUpConfig.JSON_CONFIG_FILENAME
        )

    def get_config(
        self,
        index: str,
        config_type: str,
    ) -> typing.Union[typing.Dict, typing.List]:
        """Get the specified service's configuration.

        Args:
            index (str): The configuration index.
            config_type (str): The type of configuration.

        """

        index_config: typing.Dict = self._get_index_config(index=index)
        specific_config: typing.Union[typing.Dict, typing.List] = index_config.get(
            config_type, {}
        )

        if not specific_config:
            log.warning(f"No {config_type} configuration found for {index}.")

        return specific_config

    def _get_index_config(self, index: str) -> typing.Dict:
        """Get the higher level indexed configuration.

        Args:
            index (str): The name of the config.

        Returns:
            typing.Dict: The configuration dictionary object

        """

        index_config: typing.Dict = self.json_config.get(index)
        if not index_config:
            raise ValueError(f"Service configuration not found for {index}")

        return index_config

    def _load_json_config(self, filename: str) -> typing.Dict:
        """Load the JSON configuration file.

        Args:
            filename (str): The name of the file to load

        Returns:
            typing.Dict: The configuration dictionary object from the config.json file

        """

        root: pathlib.Path = pathlib.Path(__file__).parent.parent.absolute()
        config_json_path: pathlib.Path = root / filename
        log.info(f"Loading JSON configuration from {config_json_path}")
        with open(config_json_path, "r") as file:
            json_config: typing.Dict = json.load(file)

        return json_config
