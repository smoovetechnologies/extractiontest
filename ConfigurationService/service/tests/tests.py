import os
from companyloggers.richie import log
from dotenv import load_dotenv

from configs import system
from pathlib import Path
from configs.tests_copy import version

# Get the directory of this file
BASE_DIR: Path = Path(__file__).resolve()
ROOT_DIR: Path = BASE_DIR.parent.parent.parent
ENV_PATH: Path = ROOT_DIR / "ContainerOrchestration" / ".env"
# Load the environment variables for local development
load_dotenv(dotenv_path=ENV_PATH)


def test_endpoint_mappings():
    log(version)

    # endpoint_mappings = EndpointMappings()
    # endpoint_mappings.get_base_endpoint_mappings()
    # endpoint_mappings.get_service_endpoint_mappings("TEMPLATE_SERVICE")


if __name__ == "__main__":
    test_endpoint_mappings()
